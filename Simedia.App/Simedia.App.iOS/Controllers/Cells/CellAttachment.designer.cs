// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellAttachment")]
	partial class CellAttachment
	{
		[Outlet]
		UIKit.UIButton btnOk { get; set; }

		[Outlet]
		UIKit.UIButton btnTrash { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Action ("btnOk_Click:")]
		partial void btnOk_Click (Foundation.NSObject sender);

		[Action ("btnTrash_Click:")]
		partial void btnTrash_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnTrash != null) {
				btnTrash.Dispose ();
				btnTrash = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (btnOk != null) {
				btnOk.Dispose ();
				btnOk = null;
			}
		}
	}
}
