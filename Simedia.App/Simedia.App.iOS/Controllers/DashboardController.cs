﻿using System;
using System.Linq;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using CoreAnimation;
using Simedia.App.SDK.Models;
using Simedia.App.SDK;
using FontAwesomeXamarin;

namespace Simedia.App.iOS
{
	public class DashboardController : SlideoutController, IDashboardController
	{
		#region Constructor

		public DashboardController()
			: base()
		{
			this.LeftMenuButtonText = "";
			this.RightMenuButtonText = "-";
            this.LeftMenuEnabled = false;
            this.RightMenuEnabled = true;
			this.LayerShadowing = true;
			this.StatusBarStyle = UIStatusBarStyle.LightContent;
            //this.BackgroundColor = UIColor.Red; // CoreAssumptions.COLOR_MENU_BACK.ConvertHexToColor();
			//this.NavigationControllerCreator = this.CreateNavigationController;

		}

		#endregion


		#region Properties

		public BaseUIViewController TopViewBase
		{
			get
			{
				return this.TopView as BaseUIViewController;
			}
		}
		public UIStatusBarStyle StatusBarStyle { get; protected set; }

		#endregion

		#region Overrides

		[Obsolete("This is not present for this controller", true)]
		public override UIStoryboard Storyboard
		{
			get
			{
				return base.Storyboard;
			}
		}

		public override UIStatusBarStyle PreferredStatusBarStyle()
		{
			return this.StatusBarStyle;
		}

		public override void LoadView()
		{
			CoreUtility.ExecuteMethod("LoadView", delegate
			{
				base.LoadView();
				this.LeftMenuButtonText = ""; //fallback
				this.RightMenuButtonText = "-"; //fallback

                this.TopView = SimediaAppDelegate.Current.MainStoryboard.InstantiateViewController(ProjectListController.IDENTIFIER);				
				this.MenuViewLeft = SimediaAppDelegate.Current.MainStoryboard.InstantiateViewController(DashboardMenuController.IDENTIFIER);
			});
		}

		public override void ViewDidLoad()
		{
			CoreUtility.ExecuteMethod("ViewDidLoad", delegate
			{
                SimediaAppDelegate.Current.DashboardController = this; // before base loads!
				base.ViewDidLoad();
			});
		}

		public override void ViewDidAppear(bool animated)
		{
			CoreUtility.ExecuteMethod("ViewDidAppear", delegate
			{
				base.ViewDidAppear(animated);

				SimediaAppDelegate.Current.DashboardController = this;				
			});
		}

		//protected override UIBarButtonItem CreateLeftMenuButton()
		//{
		//	return CoreUtility.ExecuteFunction("CreateLeftMenuButton", delegate
  //          {
		//		FABarButtonItem leftButton = new FABarButtonItem(FontAwesome.FABars, "", UIColor.White, delegate
		//		{
  //                  this.ShowMenuLeft();
		//		});

  //              return leftButton;
				
		//	});
		//}		

        protected override UIBarButtonItem CreateRightMenuButton()
        {
			return CoreUtility.ExecuteFunction("CreateRightMenuButton", delegate
			{
				FABarButtonItem rightButton = new FABarButtonItem(FontAwesome.FABars, "", UIColor.White, delegate
				{
					this.ShowMenuLeft();
				});

				return rightButton;

			});
        }

		protected override void BeforeShowMenuLeft()
		{
			CoreUtility.ExecuteMethod("BeforeShowMenuLeft", delegate ()
			{
				base.BeforeShowMenuLeft();

				IDashboardChildContoller childContoller = this.MenuViewLeft as IDashboardChildContoller;
				if (childContoller != null)
				{
					childContoller.BeforeShow();
				}
				string controllerTitle = "Dashboard";
				BaseUIViewController controller = this.TopView as BaseUIViewController;
				if (controller != null)
				{
					controllerTitle = controller.TrackPrefix;
				}				
			});
		}

		#endregion

		#region Public Methods

        public void ShowMainMenu()
        {
            CoreUtility.ExecuteMethod("ShowMainMenu", delegate ()
            {
                this.ShowMenuLeft();
            });
        }

        public void NavigateToProjects()
        {
            CoreUtility.ExecuteMethod("NavigateToProjects", delegate ()
            {
                ProjectListController projectsController = SimediaAppDelegate.Current.MainStoryboard.InstantiateViewController(ProjectListController.IDENTIFIER) as ProjectListController;
                SimediaAppDelegate.Current.DashboardController.SetTopView(projectsController, true);
            });
        }

        public void ScrollToTop()
		{
			CoreUtility.ExecuteMethod("ScrollToTop", delegate ()
			{
				BaseUIViewController baseController = null;

				if (LeftMenuOpen)
				{
					baseController = this.MenuViewLeft as BaseUIViewController;
				}
				else if (RightMenuOpen)
				{
					baseController = this.MenuViewRight as BaseUIViewController;
				}
				else
				{
					baseController = this.TopView as BaseUIViewController;
				}
				if (baseController != null)
				{
					baseController.ScrollToTop();
				}
			});
		}
		
        public void SetStatusBarStyle(UIStatusBarStyle statusBarStyle)
		{
			CoreUtility.ExecuteMethod("SetStatusBarStyle", delegate ()
			{
				this.StatusBarStyle = statusBarStyle;
				this.SetNeedsStatusBarAppearanceUpdate();
			});
		}

		public void CloseMenu()
		{
			CoreUtility.ExecuteMethod("CloseMenu", delegate ()
			{
				base.Hide(true);
			});
		}

		public void SetTopView(UIViewController controller, bool animated)
		{
			CoreUtility.ExecuteMethod("SetTopView", delegate ()
			{
				//TODO:Should:Animate SetTopView
				this.TopView = controller;				
			});
		}		
		
		public void RefreshMenu()
		{
			CoreUtility.ExecuteMethod("RefreshMenu", delegate ()
			{
				DashboardMenuController menuController = this.MenuViewLeft as DashboardMenuController;
				if (menuController != null)
				{
					menuController.ViewModel.DoRefreshData(true, true);
				}
			});
		}

		#endregion

		protected UINavigationController CreateNavigationController(UIViewController view)
		{
			return CoreUtility.ExecuteFunction("CreateNavigationController", delegate ()
			{
				UINavigationController result = new UINavigationController(view);
				result.Delegate = new PresentWithoutFadeDelegate();
				return result;
			});
		}

        public void SetBadge(int count)
        {
            throw new NotImplementedException();
        }

        public class PresentWithoutFadeDelegate : UINavigationControllerDelegate
		{
			public PresentWithoutFadeDelegate()
			{
				_pushAnimation = new PresentWithoutFadeAnimation(true);
				_popAnimation = new PresentWithoutFadeAnimation(false);
			}

			private PresentWithoutFadeAnimation _pushAnimation;
			private PresentWithoutFadeAnimation _popAnimation;

			public override IUIViewControllerAnimatedTransitioning GetAnimationControllerForOperation(UINavigationController navigationController, UINavigationControllerOperation operation, UIViewController fromViewController, UIViewController toViewController)
			{
				if (operation == UINavigationControllerOperation.Pop)
				{
					return _popAnimation;
				}
				else
				{
					return _pushAnimation;
				}
			}

		}

		public class PresentWithoutFadeAnimation : UIViewControllerAnimatedTransitioning
		{
			public PresentWithoutFadeAnimation(bool showing = true)
			{
				this.Showing = showing;
			}

			public bool Showing { get; set; }


			public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
			{

				UIViewController fromViewController = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);
				UIViewController toViewController = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);
				UIView containerView = transitionContext.ContainerView;
				UIView fromView = fromViewController.View;
				UIView toView = toViewController.View;

				double duration = this.TransitionDuration(transitionContext);
				nfloat viewWidth = containerView.Frame.Width;

				CGRect toViewFrame = new CGRect();

				if (this.Showing)
				{
					toViewFrame = new CGRect(viewWidth, toView.Frame.Y, toView.Frame.Width, toView.Frame.Height);
				}
				else
				{
					toViewFrame = new CGRect(-viewWidth, toView.Frame.Y, toView.Frame.Width, toView.Frame.Height);
				}
				toView.Frame = toViewFrame;

				containerView.AddSubview(toView);

				nfloat distance = 0;
				if (this.Showing)
				{
					distance = -viewWidth;
				}
				else
				{
					distance = viewWidth;
				}

				CATransaction.Begin();
				CATransaction.AnimationDuration = duration;
				CATransaction.CompletionBlock = delegate ()
				{
					toView.Frame = new CGRect(containerView.Bounds.GetMinX(), toViewFrame.Y, toViewFrame.Width, toViewFrame.Height);
					toView.Layer.RemoveAllAnimations();
					fromView.Layer.RemoveAllAnimations();
					if (!this.Showing)
					{
						fromView.RemoveFromSuperview();
					}
					transitionContext.CompleteTransition(!transitionContext.TransitionWasCancelled);
				};

				AnimationBuilder.Begin(toView.Layer)
					.SetDefaultDurationSeconds(duration)
					.SetDefaultEasingFunction(AnimationBuilder.Timing.exponentialEaseOut)
					.MoveBy(new CGPoint(distance, 0))
					.Commit();

				AnimationBuilder.Begin(fromView.Layer)
					.SetDefaultDurationSeconds(duration)
					.SetDefaultEasingFunction(AnimationBuilder.Timing.exponentialEaseOut)
					.MoveBy(new CGPoint(distance, 0))
					.Commit();

				CATransaction.Commit();

			}
			public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext)
			{
				return 0.5;
			}
		}
	}
}
