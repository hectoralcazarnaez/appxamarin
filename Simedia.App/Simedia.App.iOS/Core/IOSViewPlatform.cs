﻿using Foundation;
using SDWebImage;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Simedia.App.iOS
{
    public class IOSViewPlatform : BaseClass, IViewPlatform
    {
        public IOSViewPlatform()
            : base("IOSViewPlatform")
        {
            NSString version = new NSString("CFBundleShortVersionString");
            if (NSBundle.MainBundle.InfoDictionary.ContainsKey(version))
            {
                this.VersionNumber = NSBundle.MainBundle.InfoDictionary[version].ToString();
            }
            else
            {
                this.VersionNumber = "0.0";
            }
        }
        public virtual string VersionNumber { get; set; }

        public virtual string ShortName
        {
            get { return "ios"; }
        }
        public void OnMemoryWarning()
        {
            base.ExecuteMethod("OnMemoryWarning", delegate ()
            {
                SDWebImageManager.SharedManager.ImageCache.ClearMemory();
            });
        }
        public void NavigateToFirstScreen()
        {
            this.ExecuteMethodOnMainThread("NavigateToFirstScreen", delegate ()
            {
                SimediaAppDelegate.Current.LaunchLogin();
            });
        }
        public void OnLoggedOn()
        {
            
        }
        public void OnLoggedOff()
        {
            this.ExecuteMethodOnMainThread("OnLoggedOff", delegate ()
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                SDWebImageManager.SharedManager.ImageCache.ClearDisk();
                SDWebImageManager.SharedManager.ImageCache.ClearMemory();

            });
        }


        public void DisplayNotification(string title, string message)
        {
            this.ExecuteMethodOnMainThread("DisplayNotification", delegate ()
            {
                new UIAlertView(title, message, null, "OK").Show();
            });
        }

        private bool _showingOutdated;

#pragma warning disable 0414 // <reference keeper>
        private UIAlertView _recentAlertView;
#pragma warning restore 0414 // </reference keeper>

        public void OnOutDated(string message)
        {
            this.ExecuteMethodOnMainThread("OnOutDated", delegate ()
            {
                if (_showingOutdated) { return; }
                _showingOutdated = true;

                if (string.IsNullOrEmpty(message))
                {
                    message = "This version of the app is no longer supported.";
                }

                var view = new UIAlertView("App Expired", message + "\nPlease download the latest version from the app store.", null, "No Thanks", "OK");
                view.Dismissed += AlertViewOutDated_Dismissed;
                view.Show();
                _recentAlertView = view;
            });
        }
        private void AlertViewOutDated_Dismissed(object sender, UIButtonEventArgs e)
        {
            this.ExecuteMethodOnMainThread("AlertViewOutDated_Dismissed", delegate ()
            {
                _showingOutdated = false;
                UIAlertView alert = sender as UIAlertView;
                if (alert != null)
                {
                    alert.Dismissed -= AlertViewOutDated_Dismissed;
                }
                if (e.ButtonIndex == 1)
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(CoreAssumptions.IOS_APPSTORE_URL));
                }
            });
        }
        public void RegisterForPushNotifications()
        {
            this.ExecuteMethodOnMainThread("RegisterForPushNotifications", delegate ()
            {
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    UIUserNotificationType types = UIUserNotificationType.Sound | UIUserNotificationType.Badge | UIUserNotificationType.Alert;
                    UIUserNotificationSettings settings = UIUserNotificationSettings.GetSettingsForTypes(types, null);
                    UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
                }
                else
                {
                    UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound | UIRemoteNotificationType.Alert);
                }
            });
        }
        public void UnRegisterForPushNotifications()
        {
            this.ExecuteMethodOnMainThread("RegisterForPushNotifications", delegate ()
            {
                UIApplication.SharedApplication.UnregisterForRemoteNotifications();
            });
        }

        public void ShowToast(string message)
        {
            this.ExecuteMethodOnMainThread("ShowToast", delegate ()
            {
                HUD.ShowToast(message, false);
            });
        }

        
        public UIViewController GetRootViewController()
        {
            return base.ExecuteFunction("GetRootViewController", delegate ()
            {
                return SimediaAppDelegate.Current.AppWindow.RootViewController;
            });
        }
        public UINavigationController GetRootNavigationController()
        {
            return base.ExecuteFunction("GetRootNavigationController", delegate ()
            {
                UINavigationController navController = SimediaAppDelegate.Current.AppWindow.RootViewController as UINavigationController;
                if (navController == null)
                {
                    navController = SimediaAppDelegate.Current.AppWindow.RootViewController.NavigationController;
                }
                return navController;
            });
        }

        public void ExecuteMethodOnMainThread(string name, Action method)
        {
            UIApplication.SharedApplication.InvokeOnMainThread(new Action(delegate ()
            {
                base.ExecuteMethod(name, method);
            }));
        }

        private string _deviceInformation;
        public string GetDeviceInformation()
        {
            return base.ExecuteFunction("GetDeviceInformation", delegate ()
            {
                if (string.IsNullOrEmpty(_deviceInformation))
                {
                    _deviceInformation = new iOSHardware().GetModel(DeviceHardware.Version);
                }
                return _deviceInformation;
            });
        }
    }
}
