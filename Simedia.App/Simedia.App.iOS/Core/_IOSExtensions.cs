﻿using System;
using UIKit;

namespace Simedia.App.iOS
{
    public static class _IOSExtensions
    {
        public static UIView RoundCorners(this UIView ctl, int radious = 5)
        {
            ctl.Layer.CornerRadius = radious;
			ctl.ClipsToBounds = true;
            return ctl;
        }
    }
}
