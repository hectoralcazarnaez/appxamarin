// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellTask")]
	partial class CellTask
	{
		[Outlet]
		UIKit.UIButton btnDescription { get; set; }

		[Outlet]
		UIKit.UIButton btnExpand { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblDueDate { get; set; }

		[Outlet]
		UIKit.UILabel lblTask { get; set; }

		[Outlet]
		UIKit.UITableView tblTaskActivities { get; set; }

		[Action ("btnDescription_Click:")]
		partial void btnDescription_Click (Foundation.NSObject sender);

		[Action ("btnExpand_Click:")]
		partial void btnExpand_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnDescription != null) {
				btnDescription.Dispose ();
				btnDescription = null;
			}

			if (btnExpand != null) {
				btnExpand.Dispose ();
				btnExpand = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblDueDate != null) {
				lblDueDate.Dispose ();
				lblDueDate = null;
			}

			if (lblTask != null) {
				lblTask.Dispose ();
				lblTask = null;
			}

			if (tblTaskActivities != null) {
				tblTaskActivities.Dispose ();
				tblTaskActivities = null;
			}
		}
	}
}
