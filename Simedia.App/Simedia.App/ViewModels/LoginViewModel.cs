﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simedia.App
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginViewModel(IViewModelView viewModelView)
            :base(viewModelView, "LoginViewModel")
        {

        }

        public string Text_Username
        {
            get
            {
                if (SimediaApp.IsFirstTimeLogin)
                {
                    return "Username / Nom d'utilisateur";
                }
                else
                {
                    return SimediaApp.GetLocalizedText(LanguageToken.GEN_Username, "Username");
                }
            }
        }

		public string Text_Username_Blank
		{
			get
			{
				if (SimediaApp.IsFirstTimeLogin)
				{
                    return "Please enter username / Entrez le nom d'utilisateur";
				}
				else
				{
                    return SimediaApp.GetLocalizedText(LanguageToken.LOGIN_Username_Blank, "Please enter username");
				}				
			}
		}

		public string Text_Password
		{
			get
			{
				if (SimediaApp.IsFirstTimeLogin)
				{
                    return "Password / Mot de passe";
				}
				else
				{
                    return SimediaApp.GetLocalizedText(LanguageToken.GEN_Password, "Password");
				}				
			}
		}

		public string Text_Password_Blank
		{
			get
			{
				if (SimediaApp.IsFirstTimeLogin)
				{
                    return "Please enter password / Veuillez entrer le mot de passe";
				}
				else
				{
                    return SimediaApp.GetLocalizedText(LanguageToken.LOGIN_Password_Blank, "Please enter password");
				}				
			}
		}

		public string Text_Login
		{
			get
			{
				if (SimediaApp.IsFirstTimeLogin)
				{
                    return "Login / S'identifier";
				}
				else
				{
                    return SimediaApp.GetLocalizedText(LanguageToken.LOGIN, "Login");
				}				
			}
		}

		public string Text_Title
		{
			get
			{
				if (SimediaApp.IsFirstTimeLogin)
				{
					return "First Time Login / Première Connexion";
				}
				else
				{
					return "Company";
				}
			}
		}
    }
}
