﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Simedia.App.SDK;

namespace Simedia.App
{
    public class DashboardMenuViewModel : BaseDataViewModel<MenuItem>
    {
		public DashboardMenuViewModel(IDataViewModelView<MenuItem> viewModelView)
			: base(viewModelView, "DashboardMenuViewModel")
		{
		}

		#region Language

		public string Text_ProjectList
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.PROJECTLIST_Title, "Project list");
			}
		}

		public string Text_Logout
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.LOGIN_Logout, "Logout");
			}
		}

		public string CompanyName
		{
			get
			{
                return "Company name";
			}
		}

		#endregion

		#region Overrides

		public override int ScrollThresholdCount
		{
			get
			{
				return 0;
			}
		}

		public override int ScrollThresholdSize
		{
			get
			{
				return 0;
			}
		}

		protected override Task PerformGetMoreData(int startPage)
		{
			return Task.FromResult(0);
		}

		protected override Task PerformRefreshData(bool force)
		{
            return base.ExecuteFunction("PerformRefreshData", delegate ()
            {
				List<MenuItem> items = new List<MenuItem>();
				
                items.Add(new MenuItem()
				{
					type = "project_list",
                    Text = this.Text_ProjectList
				});
				items.Add(new MenuItem()
				{
					type = "logout",
                    Text = this.Text_Logout
				});

				this.OnDataRetrieved(new ListResult<MenuItem>(items));
				return Task.FromResult(0);
            });
		}

        #endregion


    }
}
