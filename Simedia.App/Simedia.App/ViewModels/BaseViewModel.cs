﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simedia.App
{
    public abstract class BaseViewModel : BaseClass
    {
        public BaseViewModel(IViewModelView view, string trackPrefix)
            : base(trackPrefix)
        {
            this.ViewModelView = view;
        }

        public bool HasStarted { get; set; }
        public bool FirstAppear { get; set; }

        public IViewModelView ViewModelView { get; set; }
        
        public virtual void OnAppear()
        {
            base.ExecuteMethod("OnAppear", delegate ()
            {
                this.FirstAppear = false;
            });
        }
        public virtual void OnDisappear()
        {
            base.ExecuteMethod("OnDisappear", delegate ()
            {

            });
        }

        public virtual void Start()
        {
            base.ExecuteMethod("Start", delegate ()
            {
                this.HasStarted = true;
            });
        }

        public virtual ISimediaApp SimediaApp
        {
            get
            {
                return Container.SimediaApp;
            }
        }
    }
}
