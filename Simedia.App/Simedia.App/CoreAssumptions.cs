﻿using System;
using System.Collections.Generic;
using System.Text;
using Simedia.App.SDK.Shared;

namespace Simedia.App
{
    public static partial class CoreAssumptions
    {
        static CoreAssumptions()
        {
            //init values here           
        }

        // <Provided By App Instance Partial Class>

        public static string APP_INSTANCE_NAME = "qa";        
        public static string IOS_APPSTORE_URL = "";
        public static string ANDROID_GCM_SENDER_ID = ""; // PROVIDED PER LABEL
        public static string ANDROID_ICON = ""; // PROVIDED PER LABEL
        public static AppNaming NAMING = new AppNaming();

        // </Provided By App Instance Partial Class>

        public static string INTERNAL_APP_NAME = "ACUTE 365";

        public static readonly string BASE_API_URL = "https://acute360.com";
        //public static readonly string BASE_API_URL = "https://jsonplaceholder.typicode.com";

        public static string COLOR_POPUP_BACKGROUND = "#D0D9DD";
        public static string COLOR_TABLE_SEPARATOR = "#EBEBEB";
        public static string COLOR_MENU_SEPARATOR = "#58B8E4";
        public static string COLOR_TEXTBOX_BORDER = "#E3E3E3";

        public static string COLOR_GREY_DARK = "#283339";
        public static string COLOR_GREY_MID = "#3c464b";// was: "#50595E";
        public static string COLOR_GREY_MID_LIGHT = "#50595E";
        public static string COLOR_GREY_MID_Darker = "#3c464b"; // now same as COLOR_GREY_MID
        public static string COLOR_GREY_LIGHT = "#B1BFC6";
        public static string COLOR_GREY_LIGHTER = "#D0D9DD";
        public static string COLOR_GREY_LIGHTEST = "#EDF0F1";
        public static string COLOR_GREEN_FOCUS = "#25cf5f";
        public static string COLOR_MENU_BACK = "#4F70AE";

    }
}
