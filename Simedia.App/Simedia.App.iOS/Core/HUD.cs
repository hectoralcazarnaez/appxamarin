﻿using BigTed;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Simedia.App.iOS
{
    public class HUD
    {
        public HUD()
        {
        }

        public static void ShowModal(UIView view)
        {
            UIWindow window = UIApplication.SharedApplication.KeyWindow;
            view.Frame = window.Bounds;
            window.AddSubview(view);
        }
        public static SimplePopup ShowModalWithBackground(UIView invokingControl, UIView view)
        {
            return SimplePopup.Show(invokingControl, true, view);
        }
        public static void ShowToast(string message, bool centered = true, double timeoutMS = 1000)
        {
            BTProgressHUD.ShowToast(message, centered, timeoutMS);
        }
        public static void Show(string message, bool underlay = true)
        {
            //TODO:
            if (underlay)
            {
                BTProgressHUD.Show(message, -1f, ProgressHUD.MaskType.Black);
            }
            else
            {
                BTProgressHUD.Show(message, -1f, ProgressHUD.MaskType.Clear);
            }
        }
        public static void ShowSuccessWithStatus(string status, double timeoutMs)
        {
            ProgressHUD.Shared.ShowSuccessWithStatus(status, timeoutMs);
        }
        public static void ShowErrorWithStatus(string status, double timeoutMs)
        {
            BTProgressHUD.ShowErrorWithStatus(status, timeoutMs);
        }
        public static void Dismiss()
        {
            BTProgressHUD.Dismiss();
        }
    }
}
