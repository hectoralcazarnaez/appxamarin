﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simedia.App.Services.MediaUploader
{
    public class ToastChangedEventArgs : EventArgs
    {
        public ToastChangedEventArgs(UploadToast toast)
        {
            Toast = toast;
        }

        public UploadToast Toast { get; set; }
    }
}
