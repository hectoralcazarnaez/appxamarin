// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellProject")]
	partial class CellProject
	{
		[Outlet]
		UIKit.UIButton btnMore { get; set; }

		[Outlet]
		UIKit.UIButton btnProjectDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblDueDate { get; set; }

		[Outlet]
		UIKit.UILabel lblDueDateName { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Action ("btnDescription_Click:")]
		partial void btnDescription_Click (Foundation.NSObject sender);

		[Action ("btnMore_Click:")]
		partial void btnMore_Click (Foundation.NSObject sender);

		[Action ("btnProjectDescription_Click:")]
		partial void btnProjectDescription_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnMore != null) {
				btnMore.Dispose ();
				btnMore = null;
			}

			if (btnProjectDescription != null) {
				btnProjectDescription.Dispose ();
				btnProjectDescription = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblDueDate != null) {
				lblDueDate.Dispose ();
				lblDueDate = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblDueDateName != null) {
				lblDueDateName.Dispose ();
				lblDueDateName = null;
			}
		}
	}
}
