﻿using System;
using System.Collections;
using System.Collections.Generic;
using Simedia.App.SDK.Models;

namespace Simedia.App.ViewModels
{
    public class ImageAttachmentViewModel : BaseViewModel
    {
        public ImageAttachmentViewModel(IViewModelView viewModelView)
            : base(viewModelView, "ImageAttachmentViewModel")
        {
            
        }


		#region Language
		public string Text_Title
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.IMAGEATTACHMENT_Title, "Image Attachment");
			}
		}

		public string Text_Done
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Done, "Done");
			}
		}

		public string Text_Cancel
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Cancel, "Cancel");
			}
		}

		public string Text_Description
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.IMAGEATTACHMENT_Description, "Description");
			}
		}

		public string Text_DetailedDescription
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.IMAGEATTACHMENT_DetailedDescription, "Detailed description");
			}
		}

		public string Text_DescriptionBlank
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.IMAGEATTACHMENT_DescriptionBlank, "Description cant be blank");
			}
		}

		public string Text_Close
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Close, "Close");
			}
		}
        #endregion

        public List<string> AttachmentDescription { get; set; }

        public override void Start()
        {
            base.ExecuteMethod("Start", delegate ()
            {
                base.Start();
                AttachmentDescription = new List<string>();

                foreach (var item in Enum.GetValues(typeof(Attachment_Type)))
                {
                    AttachmentDescription.Add(item.ToString().ToReadableEnumName());
                }
            });
        }
    }
}
