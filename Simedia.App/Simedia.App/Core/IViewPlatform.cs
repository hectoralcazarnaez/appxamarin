﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simedia.App
{
    public partial interface IViewPlatform
    {
        string ShortName { get; }
        string VersionNumber { get; }
        string GetDeviceInformation();

        void NavigateToFirstScreen();

        void OnLoggedOff();
        void OnLoggedOn();
        void OnOutDated(string message);


        void RegisterForPushNotifications();
        void UnRegisterForPushNotifications();

        void ShowToast(string message);
        void DisplayNotification(string title, string message);       

        void OnMemoryWarning();
    }
}
