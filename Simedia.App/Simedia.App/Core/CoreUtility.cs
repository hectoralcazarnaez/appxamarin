﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Simedia.App
{
    public static class CoreUtility
    {
        public static bool ALLOW_DISPOSE_TRACE = true; // release mode never uses this, it is always disabled in release
        public static bool ALLOW_TRACING = false; // release mode never uses this, it is always disabled in release

        public static void ExecuteMethod(string name, Action method, Action<Exception> onError = null, bool supressMethodLogging = false)
        {
            try
            {
                method();
            }
            catch (Exception ex)
            {
                Container.Track.LogError(ex, name);
                if (onError != null)
                {
                    onError(ex);
                }
                else
                {
                    HandleException(ex);
                }
            }
        }

        public static async Task ExecuteMethodAsync(string name, Func<Task> method, Action<Exception> onError = null, bool supressMethodLogging = false)
        {
#if DEBUG_METHODS
            if(!supressMethodLogging)
            {
            LogMethodTrace(string.Format("<{0}>", name));
            }
#endif
            try
            {
                await method();
            }
            catch (Exception ex)
            {
                Container.Track.LogError(ex, name);
                if (onError != null)
                {
                    onError(ex);
                }
                else
                {
                    HandleException(ex);
                }
            }
#if DEBUG_METHODS
            finally
            {
            if(!supressMethodLogging)
            {
            LogMethodTrace(string.Format("</{0}>", name));
            }
            }
#endif
        }
        public static T ExecuteFunction<T>(string name, Func<T> method, Action<Exception> onError = null, bool supressMethodLogging = false)
        {
#if DEBUG_METHODS
            if(!supressMethodLogging)
            {
                LogMethodTrace(string.Format("<{0}>", name));
            }
#endif
            try
            {
                return method();
            }
            catch (Exception ex)
            {
                Container.Track.LogError(ex, name);
                if (onError != null)
                {
                    onError(ex);
                }
                else
                {
                    HandleException(ex);
                }
                return default(T);
            }
#if DEBUG_METHODS
            finally
            {
                if(!supressMethodLogging)
                {
                LogMethodTrace(string.Format("</{0}>", name));
                }
            }
#endif
        }
        public static async Task<T> ExecuteFunctionAsync<T>(string name, Func<Task<T>> method, Action<Exception> onError = null)
        {
#if DEBUG_METHODS
            LogMethodTrace(string.Format("<{0}>", name));
#endif
            try
            {
                return await method();
            }
            catch (Exception ex)
            {
                Container.Track.LogError(ex, name);
                if (onError != null)
                {
                    onError(ex);
                }
                else
                {
                    HandleException(ex);
                }
                return default(T);
            }
#if DEBUG_METHODS
            finally
            {
            LogMethodTrace(string.Format("</{0}>", name));
            }
#endif
        }

        public static void HandleException(Exception ex)
        {
            AggregateException aggregate = ex as AggregateException;
            if (aggregate != null)
            {
                foreach (var item in aggregate.InnerExceptions)
                {
                    HandleException(item);
                }
                return;
            }

            //TODO:COULD: Process Special Exception Types
            //IE: Catch all HTTP:429 errors, etc
        }

        private static void LogMethodTrace(string message)
        {
#if DEBUG
            if (ALLOW_TRACING)
            {
                Container.Track.LogTrace(message);
            }
#endif
        }

        public static void CopyStream(Stream input, Stream output, int bufferSizeBytes = 1024)
        {
            byte[] buffer = new byte[bufferSizeBytes];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
    }
}
