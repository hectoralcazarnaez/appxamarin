﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Simedia.App.SDK.Models;

namespace Simedia.App
{
    public class ProjectListViewModel : BaseDataViewModel<Project>
    {
        public ProjectListViewModel(IDataViewModelView<Project> viewModelView)
            :base(viewModelView, "ProjectListViewModel")
        {

        }

		#region Language

        public string Text_ViewTitle
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.PROJECTLIST_Title, "PROJECT LIST");
            }
        }

		public string Text_ViewMore
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_ViewMore, "View more");
			}
		}

		public string Text_Description
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.PROJECTLIST_Description, "Description");
			}
		}

		public string Text_DueDate
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.PROJECTLIST_DueDate, "Due Date");
			}
		}

		#endregion

		public string Color_TableSeparator
		{
			get
			{
                return CoreAssumptions.COLOR_TABLE_SEPARATOR;
			}
		}

		#region Overrides

        public override int ScrollThresholdCount 
        {
            get
            {
                return this.SimediaApp.AppConfig.Projects_InfiniteThresholdCount;
            }
        }

		public override int ScrollThresholdSize 
        {
            get
            {
                return this.SimediaApp.AppConfig.Projects_InfiniteThresholdSize;
            }
        }

		protected override Task PerformGetMoreData(int startPage)
		{
            return this.SimediaApp.ProjectsFetchAsync(null, true, startPage, this.SimediaApp.AppConfig.Projects_PageSize, this.OnMoreDataRetrieved);
		}

		protected override Task PerformRefreshData(bool force)
		{
			return this.SimediaApp.ProjectsFetchAsync(null, true, 1, this.SimediaApp.AppConfig.Projects_PageSize, this.OnDataRetrieved);
		}

        #endregion


    }
}
