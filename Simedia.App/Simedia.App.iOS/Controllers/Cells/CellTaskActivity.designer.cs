// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellTaskActivity")]
	partial class CellTaskActivity
	{
		[Outlet]
		UIKit.UIButton btnActivityDescription { get; set; }

		[Outlet]
		UIKit.UIButton btnSearch { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblDueDate { get; set; }

		[Outlet]
		UIKit.UILabel lblName { get; set; }

		[Action ("btnActivityDescription_Click:")]
		partial void btnActivityDescription_Click (Foundation.NSObject sender);

		[Action ("btnDescription_Click:")]
		partial void btnDescription_Click (Foundation.NSObject sender);

		[Action ("btnSearch_Click:")]
		partial void btnSearch_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnSearch != null) {
				btnSearch.Dispose ();
				btnSearch = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}

			if (lblDueDate != null) {
				lblDueDate.Dispose ();
				lblDueDate = null;
			}

			if (btnActivityDescription != null) {
				btnActivityDescription.Dispose ();
				btnActivityDescription = null;
			}
		}
	}
}
