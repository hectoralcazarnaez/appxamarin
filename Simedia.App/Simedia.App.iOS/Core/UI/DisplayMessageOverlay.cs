﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace Simedia.App.iOS
{
    public class DisplayMessageOverlay : BaseUIView
    {
        //control declarations
        UIView viewContainer;
        UIView viewHeader;
        UILabel lblTitle;
        UITextView txtMessage;
        UIButton btnClose;

        private CGRect _parentFrame;
        private string _title { get; set; }
        private string _message { get; set; }
        private string _buttonText { get; set; }
        private bool _editable { get; set; }

        public event EventHandler<FinishEditingEventArgs> OnFinishEditing;

        public DisplayMessageOverlay(CGRect frame, string title, string message, string buttonText, bool editable = false, bool viewOnTop = false)
            : base(frame)
        {
            _parentFrame = frame;
            _title = title;
            _message = message;
            _buttonText = buttonText;
            _editable = editable;

            BackgroundColor = UIColor.Black.ColorWithAlpha(0.75f);
            AutoresizingMask = UIViewAutoresizing.All;

            nfloat centerX = Frame.Width / 2;
            nfloat centerY = Frame.Height / 2;
            //create main container
            nfloat containerHeight = this._parentFrame.Height - 350;
            nfloat containerWidth = this._parentFrame.Width - 40;

            nfloat yPosition;

            if (viewOnTop)
            {
                yPosition = 60f;
            }
            else
            {
                yPosition = centerY - (containerHeight / 2) - 100;
            }

            this.viewContainer = new UIView();
            this.viewContainer.Frame = new CGRect(
                centerX - (containerWidth / 2),
                yPosition,
                containerWidth,
                containerHeight
            );
            this.viewContainer.BackgroundColor = UIColor.White;
            this.viewContainer.AutoresizingMask = UIViewAutoresizing.All;
            this.viewContainer.RoundCorners();
            AddSubview(this.viewContainer);


            //create header view
            this.viewHeader = new UIView();
            this.viewHeader.Frame = new CGRect(
                0,
                0,
                this.viewContainer.Frame.Width,
                60
            );
            this.viewHeader.BackgroundColor = CoreAssumptions.COLOR_MENU_BACK.ConvertHexToColor();
            this.viewHeader.AutoresizingMask = UIViewAutoresizing.All;
            this.viewContainer.Add(this.viewHeader);


            //create header label
            this.lblTitle = new UILabel();
            this.lblTitle.Frame = new CGRect(
                10,
                15,
                this.viewHeader.Frame.Width - 20,
                30
            );
            this.lblTitle.Text = _title;
            this.lblTitle.BackgroundColor = UIColor.Clear;
            this.lblTitle.TextColor = UIColor.White;
            this.lblTitle.Font = UIFont.FromName("HelveticaNeue-Medium", 14f);
            this.lblTitle.TextAlignment = UITextAlignment.Center;
            this.lblTitle.AutoresizingMask = UIViewAutoresizing.All;
            this.viewHeader.Add(this.lblTitle);


            //create footer exit button
            nfloat buttonWidth = 130;
            nfloat buttonHeight = 40;

            this.btnClose = new UIButton();
            this.btnClose.Frame = new CGRect(
                (this.viewContainer.Frame.Width / 2) - (buttonWidth / 2),
                (this.viewContainer.Frame.Height - buttonHeight - 15),
                buttonWidth,
                buttonHeight
            );
            this.btnClose.SetTitle(_buttonText, UIControlState.Normal);
            this.btnClose.Font = UIFont.FromName("HelveticaNeue", 15f);
            this.btnClose.BackgroundColor = CoreAssumptions.COLOR_MENU_BACK.ConvertHexToColor();
            this.btnClose.SetTitleColor(UIColor.White, UIControlState.Normal);
            this.btnClose.TouchUpInside += delegate
            {
                this.Hide();
            };
            this.btnClose.RoundCorners();
            this.viewContainer.Add(this.btnClose);


            //create text view
            this.txtMessage = new UITextView();
            this.txtMessage.Frame = new CGRect(
                10,
                this.viewHeader.Frame.Height + 10,
                this.viewContainer.Frame.Width - 20,
                this.btnClose.Frame.Y - this.viewHeader.Frame.Height - 25
            );
            this.txtMessage.Layer.BorderColor = CoreAssumptions.COLOR_TEXTBOX_BORDER.ConvertHexToColor().CGColor;
            this.txtMessage.Layer.BorderWidth = 1f;
            this.txtMessage.Editable = editable;
            this.txtMessage.Text = _message;
            this.viewContainer.Add(this.txtMessage);

            if (editable)
            {
                this.txtMessage.BecomeFirstResponder();
            }

        }

        public void Hide()
        {
            if (_editable)
            {
                FinishEditingEventArgs args = new FinishEditingEventArgs(txtMessage.Text.Trim());
                this.OnFinishEditing(this, args);
            }

			UIView.Animate(
				0.5, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
        }
    }

    public class FinishEditingEventArgs : EventArgs
    {
        public string Message { get; protected set; }

        public FinishEditingEventArgs(string message)
        {
            Message = message;
        }
    }
}
