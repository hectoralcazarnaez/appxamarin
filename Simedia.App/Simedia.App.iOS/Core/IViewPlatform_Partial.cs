﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace Simedia.App
{
    public partial interface IViewPlatform
    {
        UINavigationController GetRootNavigationController();
        UIViewController GetRootViewController();
        void ExecuteMethodOnMainThread(string name, Action method);
    }
}