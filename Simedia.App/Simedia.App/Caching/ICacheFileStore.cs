﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Simedia.App.Caching
{
	public interface ICacheFileStore
	{
		bool Exists(string filePath);
		bool TryReadTextFile(string filePath, out string contents);
		void EnsureFolderExists(string folderPath);
		void WriteFile(string filePath, string contents);
		void DeleteFile(string filePath);
		void DeleteFolder(string folderPath, bool recursive);
		Stream OpenRead(string path);
		Stream OpenWrite(string path);
	}
}
