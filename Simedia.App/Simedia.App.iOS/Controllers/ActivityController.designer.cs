// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("ActivityController")]
	partial class ActivityController
	{
		[Outlet]
		UIKit.UIButton btnAddTasklog { get; set; }

		[Outlet]
		UIKit.UIButton btnEditComment { get; set; }

		[Outlet]
		UIKit.UIButton btnInfoSummary { get; set; }

		[Outlet]
		UIKit.UIButton btnSave { get; set; }

		[Outlet]
		UIKit.UILabel lblAddComment { get; set; }

		[Outlet]
		UIKit.UILabel lblAddTasklog { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogComments { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogDate { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogId { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogList { get; set; }

		[Outlet]
		UIKit.UILabel lblValidation { get; set; }

		[Outlet]
		UIKit.UITableView tblTasklog { get; set; }

		[Outlet]
		UIKit.UITextView txtComments { get; set; }

		[Action ("btnAddTasklog_Click:")]
		partial void btnAddTasklog_Click (Foundation.NSObject sender);

		[Action ("btnEditComment_Click:")]
		partial void btnEditComment_Click (Foundation.NSObject sender);

		[Action ("btnInfoSummary_Click:")]
		partial void btnInfoSummary_Click (Foundation.NSObject sender);

		[Action ("btnSave_Click:")]
		partial void btnSave_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAddTasklog != null) {
				btnAddTasklog.Dispose ();
				btnAddTasklog = null;
			}

			if (btnInfoSummary != null) {
				btnInfoSummary.Dispose ();
				btnInfoSummary = null;
			}

			if (btnSave != null) {
				btnSave.Dispose ();
				btnSave = null;
			}

			if (lblAddComment != null) {
				lblAddComment.Dispose ();
				lblAddComment = null;
			}

			if (lblAddTasklog != null) {
				lblAddTasklog.Dispose ();
				lblAddTasklog = null;
			}

			if (lblTasklogComments != null) {
				lblTasklogComments.Dispose ();
				lblTasklogComments = null;
			}

			if (lblTasklogDate != null) {
				lblTasklogDate.Dispose ();
				lblTasklogDate = null;
			}

			if (lblTasklogId != null) {
				lblTasklogId.Dispose ();
				lblTasklogId = null;
			}

			if (lblTasklogList != null) {
				lblTasklogList.Dispose ();
				lblTasklogList = null;
			}

			if (lblValidation != null) {
				lblValidation.Dispose ();
				lblValidation = null;
			}

			if (tblTasklog != null) {
				tblTasklog.Dispose ();
				tblTasklog = null;
			}

			if (txtComments != null) {
				txtComments.Dispose ();
				txtComments = null;
			}

			if (btnEditComment != null) {
				btnEditComment.Dispose ();
				btnEditComment = null;
			}
		}
	}
}
