﻿using CoreGraphics;
using Foundation;
using System;
using System.Threading.Tasks;
using UIKit;

namespace Simedia.App.iOS
{
    public class UIRefreshControlDelayed : UIRefreshControl
    {
        public UIRefreshControlDelayed()
            : base()
        {
            this.MillisecondDelayStart = 350;
        }
        public UIRefreshControlDelayed(IntPtr handle)
            : base(handle)
        {
            this.MillisecondDelayStart = 350;
        }

        public UIRefreshControlDelayed(NSObjectFlag t)
            : base(t)
        {
            this.MillisecondDelayStart = 350;
        }
        public UIRefreshControlDelayed(NSCoder coder)
            : base(coder)
        {
            this.MillisecondDelayStart = 350;
        }

        private bool _shouldStartRefresh;

        public UIScrollView ScrollView { get; set; }
        public int MillisecondDelayStart { get; set; }

        public override void BeginRefreshing()
        {
            CoreUtility.ExecuteMethodAsync("BeginRefreshing", async delegate ()
            {
                _shouldStartRefresh = true;
                await Task.Delay(this.MillisecondDelayStart);
                if (_shouldStartRefresh)
                {
                    base.BeginRefreshing();
                    UIScrollView tableViewToAdjust = this.ScrollView;
                    if (tableViewToAdjust != null && tableViewToAdjust.ContentOffset.Y == 0)
                    {
                        UIView.Animate(.25, delegate ()
                        {
                            tableViewToAdjust.ContentOffset = new CGPoint(0, -this.Frame.Size.Height);
                        });
                    }
                }
            });
        }
        public override void EndRefreshing()
        {
            _shouldStartRefresh = false;
            base.EndRefreshing();
        }
    }
}
