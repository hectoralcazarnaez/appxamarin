﻿using System;
using System.Collections.Generic;
using Simedia.App.SDK;
using Simedia.App.Caching;
using System.Threading.Tasks;

namespace Simedia.App
{
	public abstract class BaseDataViewModel<T> : BaseViewModel
	{
		public BaseDataViewModel(IDataViewModelView<T> view, string trackPrefix)
			: base(view, trackPrefix)
		{
			this.DataViewModelView = view;
			this.Data = new List<T>();
		}

		#region Language

		public string Text_General_Overdue
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Overdue, "OVERDUE");
			}
		}		

		public string Text_General_Ongoing
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Ongoing, "ONGOING");
			}
		}

		public string Text_General_UnableToLoad
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_UnableToLoadItem, "Unable to load requested item.");
			}
		}
		public string Text_General_LoadingItem
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_LoadingItem, "Loading item..");
			}
		}

		#endregion

		#region Public Properties

		public virtual List<T> Data { get; set; }
		public virtual IDataViewModelView<T> DataViewModelView { get; set; }

		public virtual bool SupressRefreshDataOnAppear { get; set; }

		public virtual bool HasMoreData { get; set; }
		public virtual int NextStartPage { get; set; }
		public virtual bool ShowNoData { get; set; }

		public virtual bool AllowPagedStaleData { get; set; }

		#endregion

		#region Protected Methods

		protected virtual Guid DataTrackerRefresh { get; set; }
		protected virtual Guid DataTrackerMore { get; set; }

		#endregion

		#region Abstract Methods

		protected abstract Task PerformRefreshData(bool force);
		protected abstract Task PerformGetMoreData(int startPage);

		public abstract int ScrollThresholdCount { get; }
		public abstract int ScrollThresholdSize { get; }

		#endregion

		#region Public Methods

		public Task DoRefreshData(bool force, bool suppressRefreshing = false, Action<bool> onFetching = null)
		{
			return base.ExecuteMethodOrSkipAsync("DoRefreshData", async delegate ()
			{
				if (!suppressRefreshing)
				{
					this.OnDataRefreshing(true);
				}

				if (onFetching != null)
				{
					onFetching(true);
				}
				this.DataTrackerRefresh = Guid.NewGuid();

				try
				{
					await PerformRefreshData(force);
				}
				finally
				{
					if (!suppressRefreshing)
					{
						this.OnDataRefreshing(false);
					}
					if (onFetching != null)
					{
						onFetching(false);
					}
				}
			});
		}
#if __IOS__

        public Task DoGetMoreData(Simedia.App.iOS.Data.IScrollListener scrollListener)
		{
			return base.ExecuteMethodAsync("DoGetMoreDataIos", async delegate ()
			{
				if (!this.HasMoreData) { return; }

				await DoGetMoreData(false);

				if (scrollListener != null)
				{
					scrollListener.ListeningDisabled = (!this.HasMoreData); // assumes OnMoreDataRetrieved is properly called 
				}
			});
		}

#endif

		public Task DoGetMoreData()
		{
			return DoGetMoreData(false);
		}
		public Task DoGetMoreData(bool suppressRefreshing)
		{
			return base.ExecuteMethodOrSkipAsync("DoGetMoreData", async delegate ()
			{
				if (!this.HasMoreData)
				{
					this.LogTrace("Tried to get more when we have no more!");
					return;
				}
				if (!suppressRefreshing)
				{
					this.OnDataRefreshing(true);
				}

				this.DataTrackerMore = this.DataTrackerRefresh;

				try
				{
					await PerformGetMoreData(this.NextStartPage);
				}
				catch (Exception ex)
				{
					this.LogError(ex, "DoGetMoreData");
				}
				finally
				{
					if (!suppressRefreshing)
					{
						this.OnDataRefreshing(false);
					}
				}

			});
		}

		public override void OnAppear()
		{
			base.ExecuteMethod("OnAppear", delegate ()
			{
				if (this.FirstAppear || !this.SupressRefreshDataOnAppear)
				{
					this.DoRefreshData(this.FirstAppear);
				}
				this.SupressRefreshDataOnAppear = false;
				base.OnAppear();
			});
		}

		public override void Start()
		{
			base.ExecuteMethod("Start", delegate ()
			{
				this.DoRefreshData(false);

				base.Start();
			});
		}

		public virtual void ReBindData()
		{
			base.ExecuteMethod("ReBindData", delegate ()
			{
				this.DataViewModelView.BindData(true, this.Data);
			});
		}

		#endregion

		#region Protected Methods

		protected virtual void OnDataRefreshing(bool refreshing)
		{
			base.ExecuteMethod("OnDataRefreshing", delegate ()
			{
				if (refreshing)
				{
					this.ShowNoData = false;
				}
				this.DataViewModelView.OnDataRefreshing(refreshing);
			});
		}

		protected virtual void OnDataRetrieved(bool live_data, ListResult<T> data)
		{
			base.ExecuteMethod("OnDataRetrieved", delegate ()
			{
				this.Data = data.items;

				if (data != null && data.paging != null)
				{
					this.HasMoreData = data.paging.total_pages > data.paging.current_page;
					this.NextStartPage = (int)data.paging.current_page + 1;

					this.ShowNoData = (this.Data.Count == 0);
				}

				this.DataViewModelView.BindData(live_data, this.Data);
			});
		}
		protected virtual void OnDataRetrieved(ListResult<T> data)
		{
			base.ExecuteMethod("OnDataRetrieved", delegate ()
			{
				this.Data = new List<T>(data.items);

				if (data != null && data.paging != null)
				{
					this.HasMoreData = data.paging.total_pages > data.paging.current_page;
					this.NextStartPage = (int)data.paging.current_page + 1;

					this.ShowNoData = (this.Data.Count == 0);
				}
				this.DataViewModelView.BindData(true, this.Data);
			});
		}

		protected virtual void OnMoreDataRetrieved(ListResult<T> items)
		{
			this.OnMoreDataRetrieved(true, items);
		}
		protected virtual void OnMoreDataRetrieved(bool isLiveData, ListResult<T> data)
		{
			base.ExecuteMethod("OnMoreDataRetrieved", delegate ()
			{
				if (isLiveData || this.AllowPagedStaleData)
				{
					if (this.DataTrackerMore != this.DataTrackerRefresh)
					{
						// we've changed, don't add
						return;
					}
					if (data != null && data.paging != null)
					{
						this.HasMoreData = data.paging.total_pages > data.paging.current_page;
						this.NextStartPage = (int)data.paging.current_page + 1;
					}
					if (data != null && data.items != null)
					{
						this.Data.AddRange(data.items);
					}
					this.DataViewModelView.AddData(data.items);
				}
			});
		}

		#endregion

	}
}
