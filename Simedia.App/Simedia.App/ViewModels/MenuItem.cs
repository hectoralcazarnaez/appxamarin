﻿using System;
namespace Simedia.App
{
    public class MenuItem
    {
        public MenuItem()
        {
        }

        public string Text { get; set; }
        public string type { get; set; }
        public object RelatedObject { get; set; }
    }
}
