﻿using Simedia.App.Services.MediaUploader;
using System;
using System.Collections.Generic;
using System.Text;
using Simedia.App.Caching;

namespace Simedia.App
{
    public static partial class Container
    {
        static Container()
        {
            Container.Track = new CoreTrack();
        }

        public static ICacheFileStore FileStore;
        public static ITrack Track;
        public static IViewPlatform ViewPlatform;
        public static IDataCache DataCache;
        public static ICacheHost CacheHost;
        public static ISimediaApp SimediaApp;
        public static Func<IMediaUploader> MediaUploader = delegate
        {
            return _mediaUploader;
        };
        private static IMediaUploader _mediaUploader;

        /// <summary>
        /// Not the standard IoC, but it'll work just fine
        /// </summary>
        public static void RegisterDependencies(ISimediaApp simediaApp, IViewPlatform platform, IMediaUploader mediaUploader, ICacheFileStore fileStore, ICacheHost cacheHost, IDataCache dataCache)
        {
            Container.ViewPlatform = platform;
            Container.SimediaApp = simediaApp;            
            Container._mediaUploader = mediaUploader;
            Container.FileStore = fileStore;
            Container.CacheHost = cacheHost;
            Container.DataCache = dataCache;
        }
    }
}
