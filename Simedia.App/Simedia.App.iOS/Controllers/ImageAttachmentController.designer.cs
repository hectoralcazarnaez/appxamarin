// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("ImageAttachmentController")]
	partial class ImageAttachmentController
	{
		[Outlet]
		UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		UIKit.UIButton btnDone { get; set; }

		[Outlet]
		UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		UIKit.UILabel lblDetailed { get; set; }

		[Outlet]
		UIKit.UITextField txtDescription { get; set; }

		[Outlet]
		UIKit.UITextView txtDetailed { get; set; }

		[Outlet]
		UIKit.UIView vwContainer { get; set; }

		[Action ("btnCancel_Click:")]
		partial void btnCancel_Click (Foundation.NSObject sender);

		[Action ("btnDone_Click:")]
		partial void btnDone_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}

			if (btnDone != null) {
				btnDone.Dispose ();
				btnDone = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblDetailed != null) {
				lblDetailed.Dispose ();
				lblDetailed = null;
			}

			if (txtDescription != null) {
				txtDescription.Dispose ();
				txtDescription = null;
			}

			if (txtDetailed != null) {
				txtDetailed.Dispose ();
				txtDetailed = null;
			}

			if (vwContainer != null) {
				vwContainer.Dispose ();
				vwContainer = null;
			}
		}
	}
}
