﻿using System;
using System.Threading.Tasks;
using Simedia.App.SDK.Models;

namespace Simedia.App.ViewModels
{
    public class TaskListViewModel : BaseDataViewModel<TaskObject>
    {
        public Project RouteProject { get; protected set; }

        public TaskListViewModel(IDataViewModelView<TaskObject> viewModelView, Project project)
            :base(viewModelView, "TaskListViewModel")
        {
            this.RouteProject = project;
		}

		#region Language

		public string Text_ViewTitle
		{
			get
			{
                return this.RouteProject.Name;
			}
		}

		public string Text_Close
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Close, "Close");
			}
		}

		public string Text_Description
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLIST_Description, "Description");
			}
		}

		#endregion

		public override int ScrollThresholdCount
		{
			get
			{
				return this.SimediaApp.AppConfig.Tasks_InfiniteThresholdCount;
			}
		}

		public override int ScrollThresholdSize
		{
			get
			{
				return this.SimediaApp.AppConfig.Tasks_InfiniteThresholdSize;
			}
		}

		protected override Task PerformGetMoreData(int startPage)
		{
            return this.SimediaApp.TasksFetchAsync(null, true, startPage, this.SimediaApp.AppConfig.Tasks_PageSize, this.OnMoreDataRetrieved);
		}

		protected override Task PerformRefreshData(bool force)
		{
			return this.SimediaApp.TasksFetchAsync(null, true, 1, this.SimediaApp.AppConfig.Tasks_PageSize, this.OnDataRetrieved);
		}
    }
}
