﻿using System;
using UIKit;
using Simedia.App.SDK.Models;

namespace Simedia.App.iOS
{
	public interface IDashboardController
	{
		void SetStatusBarStyle(UIStatusBarStyle statusBarStyle);
		void CloseMenu();
		void SetTopView(UIViewController controller, bool animated);
		UIViewController TopView { get; }
		BaseUIViewController TopViewBase { get; }
		void SetBadge(int count);
		void RefreshMenu();
		void ScrollToTop();
        void ShowMainMenu();
	}
}
