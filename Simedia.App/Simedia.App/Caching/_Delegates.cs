﻿using System;
namespace Simedia.App.Caching
{
    public delegate void DataFetchedDelegate<T>(bool isFreshData, T foundData);
}
