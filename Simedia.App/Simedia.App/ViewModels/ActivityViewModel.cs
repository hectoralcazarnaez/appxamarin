﻿using System;
using Simedia.App.SDK.Models;
using System.Collections.Generic;

namespace Simedia.App.ViewModels
{
    public class ActivityViewModel : BaseViewModel
    {
        public ActivityViewModel(IViewModelView viewModelView, Activity activity)
            : base(viewModelView, "ActivityViewModel")
        {
            this.Activity = activity;
        }

        #region Language

        public string Text_TaskLog_List
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_List, "TASK LOG LIST");
            }
        }

        public string Text_TaskLog_Id
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Id, "ID");
            }
        }

        public string Text_TaskLog_Date
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Date, "DATE");
            }
        }

        public string Text_TaskLog_Comments
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Comments, "COMMENTS");
            }
        }

        public string Text_TaskLog_Add
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_AddNew, "Add new tasklog");
            }
        }

        public string Text_Activity_Validation
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.ACTIVITY_ValSummary, "Validation / Summary");
            }
        }

        public string Text_Activity_Comments
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.ACTIVITY_Comments, "Comments");
            }
        }

        public string Text_Activity_AddComment
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.ACTIVITY_AddComment, "Add comment");
            }
        }

        public string Text_Save
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Save, "Save");
            }
        }

		public string Text_Close
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Close, "Close");
			}
		}

        #endregion

        #region Public properties

        public Activity Activity { get; set; }
        public List<TaskLog> Tasklogs
        {
            get
            {
                return Activity.Tasklogs;
            }
        }
        #endregion

        #region Overrides

        public override void Start()
        {
            base.ExecuteMethod("Start", delegate ()
            {
                base.Start();

                if (this.Activity.Tasklogs == null)
                {
                    this.Activity.Tasklogs = new List<TaskLog>();
                }
            });
        }

        #endregion

    }
}
