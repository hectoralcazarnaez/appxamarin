﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simedia.App
{
    public static partial class CoreAssumptions
    {
        public static void InitializeForSimedia()
        {
            CoreAssumptions.ANDROID_ICON = "simedia";
            CoreAssumptions.APP_INSTANCE_NAME = "sm";            
            CoreAssumptions.IOS_APPSTORE_URL = "itms-apps://itunes.apple.com/app/XXXXXXXX";
        }
    }
}
