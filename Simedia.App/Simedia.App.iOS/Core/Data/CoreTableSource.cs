﻿using System;
using System.Linq;
using UIKit;
using Foundation;
using System.Collections.Generic;
using System.Collections;
using CoreGraphics;
using CoreAnimation;

namespace Simedia.App.iOS.Data
{
	public class CoreTableSource : UITableViewSource, ICoreScrollView
	{
		public CoreTableSource()
		{
			this.CachedCustomViews = new Dictionary<string, List<UIView>>();
		}
		public CoreTableSource(string defaultCellIdentifier)
			: this()
		{
			this.CellIdentifier = defaultCellIdentifier;
		}
		public CoreTableSource(IEnumerable items, string defaultCellIdentifier, Action<object, NSIndexPath, UITableViewCell> onSelected, Action<UITableViewCell, object, NSIndexPath> binding)
			: this()
		{
			this.Items = new List<object>();
			foreach (var item in items)
			{
				this.Items.Add(item);
			}
			this.CellIdentifier = defaultCellIdentifier;
			this.OnSelectedMethod = onSelected;
			this.CustomBinding = binding;
		}

		public virtual bool DisableTabBarTracking { get; set; }

		public const string DRAG_ANIMATION_NAME = "DragHideBars";
		public virtual IScrollListener ScrollListener { get; set; }
		public virtual List<object> Items { get; set; }
		public virtual bool DisableRowDeselection { get; set; }
		public virtual bool CreateSectionPerRow { get; set; }
		public virtual nint? DefaultHeaderHeight { get; set; }
		public virtual nint? DefaultFooterHeight { get; set; }
		public Dictionary<string, List<UIView>> CachedCustomViews { get; set; }
		/// <summary>
		/// If set, during scrolling, it will auto hide.
		/// Keep protected since we need event handlers with it
		/// </summary>
		protected virtual UITabBarController AutoHideTabBarController { get; set; }
		protected virtual BaseUIViewController AutoHideControllerInstance { get; set; }

		protected virtual bool SuspendTabBarTracking { get; set; }
		public virtual CGRect? OriginalTabBarFrame { get; set; }

		protected virtual nfloat TabBarLastOffsetY { get; set; }
		public virtual bool AutoHideTabBarShouldBeHidden { get; set; }
		protected virtual string CellIdentifier { get; set; }
		protected virtual Action<object, NSIndexPath, UITableViewCell> OnSelectedMethod { get; set; }
		protected virtual Action<CoreTableSource, UITableView, NSIndexPath, UITableViewCell> OnSelectedMethodRaw { get; set; }
		protected virtual Action<UITableViewCell, object, NSIndexPath> CustomBinding { get; set; }
		protected virtual Func<UITableView, object, NSIndexPath, UITableViewCell> CustomCreator { get; set; }
		protected virtual Func<UITableView, NSIndexPath, UITableViewCell> CustomCreatorRaw { get; set; }
		protected virtual Func<CoreTableSource, UITableView, nint, UIView> CustomHeaderCreator { get; set; }
		protected virtual Func<CoreTableSource, UITableView, nint, UIView> CustomFooterCreator { get; set; }
		protected virtual Func<UITableView, object, NSIndexPath, nfloat> CustomRowSizer { get; set; }
		protected virtual Func<UITableView, NSIndexPath, nfloat> CustomRowSizerRaw { get; set; }

		protected virtual Func<UITableView, nint, nfloat> CustomHeaderSizerRaw { get; set; }
		protected virtual Func<UITableView, object, nint, nfloat> CustomHeaderSizer { get; set; }
		protected virtual Func<UITableView, nint, nfloat> CustomFooterSizer { get; set; }

		protected virtual Action<UITableView, UITableViewCell, NSIndexPath> CustomCellLayout { get; set; }
		protected virtual Func<UITableView, nint, nint> CustomRowsInSection { get; set; }
		protected virtual Func<UITableView, nint> CustomNumberOfSections { get; set; }
		public Func<UITableView, NSIndexPath, bool> CustomCanDelete { get; set; }
		public Func<UITableView, NSIndexPath, UITableViewRowAction[]> CustomRowActions { get; set; }
		public Action<UITableView, NSIndexPath> CustomRowDelete { get; set; }
		public Func<UITableView, NSIndexPath, string> CustomRowDeleteText { get; set; }
		public string DeleteText { get; set; }

		#region ICoreScrollView Members

		public event EventHandler ViewScrolled;
		protected virtual void OnViewScrolled(UIScrollView scrollView)
		{
			EventHandler handler = ViewScrolled;
			if (handler != null)
			{
				handler(scrollView, EventArgs.Empty);
			}
		}

		#endregion



		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.CustomCanDelete = null;
				this.CustomRowDelete = null;
				this.CustomRowDeleteText = null;
				this.ScrollListener = null;
				this.Items = null;
				this.OnSelectedMethod = null;
				this.CustomBinding = null;
				this.CustomCreator = null;
				this.CustomRowSizer = null;
				this.CustomRowSizerRaw = null;
				this.CustomFooterSizer = null;
				this.CustomCellLayout = null;
				this.CustomHeaderCreator = null;
				this.CustomFooterCreator = null;
				this.CustomHeaderSizer = null;
				this.CustomHeaderSizerRaw = null;
				this.CustomNumberOfSections = null;
				this.CustomRowsInSection = null;

				if (this.CachedCustomViews != null)
				{
					foreach (var list in CachedCustomViews.Values)
					{
						foreach (var item in list)
						{
							item.Dispose();
						}
						list.Clear();
					}
					this.CachedCustomViews.Clear();
					this.CachedCustomViews = null;
				}

				if (this.AutoHideTabBarController != null)
				{
					this.AutoHideTabBarController.ViewControllerSelected -= AutoHideTabBar_ControllerSelected;
					this.AutoHideTabBarController = null;

				}
			}
			base.Dispose(disposing);
		}
		public virtual void EnqueueReusableCustomView(UIView view, string identifier)
		{
			CoreUtility.ExecuteMethod("EnqueueReusableCustomView", delegate ()
			{
				if (!this.CachedCustomViews.ContainsKey(identifier))
				{
					this.CachedCustomViews[identifier] = new List<UIView>();
				}
				this.CachedCustomViews[identifier].Add(view);
			});
		}
		public virtual T DequeueReusableCustomView<T>(UITableView tableView, string identifier)
			where T : UIView, new()
		{
			return CoreUtility.ExecuteFunction("DequeueReusableCustomView", delegate ()
			{
				T result = null;
				if (!this.CachedCustomViews.ContainsKey(identifier))
				{
					this.CachedCustomViews[identifier] = new List<UIView>();
				}
				result = (T)this.CachedCustomViews[identifier].FirstOrDefault(x => x.Superview == null);
				if (tableView != null && result == null)
				{
					result = (T)(object)tableView.DequeueReusableCell(identifier);

					if (result != null)
					{
						this.CachedCustomViews[identifier].Add(result);
					}
				}
				if (result == null)
				{
					result = new T();
					if (result != null)
					{
						this.CachedCustomViews[identifier].Add(result);
					}
				}

				return result;
			});
		}
		public override UITableViewRowAction[] EditActionsForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction("UITableViewCellEditingStyle", delegate ()
			{
				if (this.CustomRowActions != null)
				{
					return this.CustomRowActions(tableView, indexPath);
				}
				return null;
			});
		}
		public override UITableViewCellEditingStyle EditingStyleForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction("UITableViewCellEditingStyle", delegate ()
			{
				if (this.CustomCanDelete != null && this.CustomCanDelete(tableView, indexPath))
				{
					return UITableViewCellEditingStyle.Delete;
				}
				return UITableViewCellEditingStyle.None;
			});
		}
		public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction("UITableViewCellEditingStyle", delegate ()
			{
				if (this.CustomCanDelete != null || this.CustomRowActions != null)
				{
					return true;
				}
				return false;
			});
		}


		public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction("TitleForDeleteConfirmation", delegate ()
			{
				if (CustomRowDeleteText != null)
				{
					string text = CustomRowDeleteText(tableView, indexPath);
					if (!string.IsNullOrEmpty(text))
					{
						return text;
					}
				}
				if (!string.IsNullOrEmpty(this.DeleteText))
				{
					return this.DeleteText;
				}
				return "Delete";
			});
		}
		public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			CoreUtility.ExecuteMethod("CommitEditingStyle", delegate ()
			{
				if (editingStyle == UITableViewCellEditingStyle.Delete)
				{
					if (CustomRowDelete != null)
					{
						CustomRowDelete(tableView, indexPath);
					}
				}
			});
		}
		public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
		{
			CoreUtility.ExecuteMethod("WillDisplay", delegate ()
			{
				if (this.CustomCellLayout != null)
				{
					this.CustomCellLayout(tableView, cell, indexPath);
				}
			});
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction<nfloat>("GetHeightForRow", delegate ()
			{
				if (this.CustomRowSizerRaw != null)
				{
					return this.CustomRowSizerRaw(tableView, indexPath);
				}
				if (this.CustomRowSizer != null)
				{
					object item = null;
					if (CreateSectionPerRow)
					{
						item = this.Items[indexPath.Section];
					}
					else
					{
						item = this.Items[indexPath.Row];
					}
					return this.CustomRowSizer(tableView, item, indexPath);
				}
				return tableView.RowHeight;
			});
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			if (CreateSectionPerRow)
			{
				if (this.Items != null)
				{
					return this.Items.Count;
				}
			}
			if (CustomNumberOfSections != null)
			{
				return CustomNumberOfSections(tableView);
			}
			return 1;
		}
		public override nint RowsInSection(UITableView tableview, nint section)
		{
			if (CreateSectionPerRow)
			{
				if (this.Items != null && this.Items.Count > 0)
				{
					return 1;
				}
			}
			else
			{
				if (CustomRowsInSection != null)
				{
					return CustomRowsInSection(tableview, section);
				}
				else if (this.Items != null)
				{
					return this.Items.Count;
				}
			}
			return 0;
		}
		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			return CoreUtility.ExecuteFunction<UITableViewCell>("GetCell", delegate ()
			{
				try
				{
					object item = null;

					UITableViewCell cell = null;

					if (this.CustomCreatorRaw != null)
					{
						return this.CustomCreatorRaw(tableView, indexPath);
					}

					if (CreateSectionPerRow)
					{
						item = this.Items[indexPath.Section];
					}
					else
					{
						item = this.Items[indexPath.Row];
					}
					if (this.CustomCreator != null)
					{
						cell = this.CustomCreator(tableView, item, indexPath);
					}
					if (cell == null)
					{
						cell = tableView.DequeueReusableCell(this.CellIdentifier);
					}
					if (cell == null)
					{
						cell = new UITableViewCell();
					}
					if (this.CustomBinding == null && this.CustomCreator == null)
					{
						cell.TextLabel.Text = item.ToString();
					}
					else
					{
						if (this.CustomBinding != null)
						{
							this.CustomBinding(cell, item, indexPath);
						}
					}
					return cell;
				}
				catch (Exception ex)
				{
					Container.Track.LogError(ex, "GetCell");
					return new UITableViewCell(); // prevent crash
				}
			});

		}
		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			return CoreUtility.ExecuteFunction("GetViewForHeader", delegate ()
			{
				UIView header = null;

				if (this.CustomHeaderCreator != null)
				{
					header = this.CustomHeaderCreator(this, tableView, section);
				}

				return header;
			});
		}
		public override UIView GetViewForFooter(UITableView tableView, nint section)
		{
			return CoreUtility.ExecuteFunction("GetViewForHeader", delegate ()
			{
				UIView footer = null;

				if (this.CustomFooterCreator != null)
				{
					footer = this.CustomFooterCreator(this, tableView, section);
				}

				return footer;
			});
		}
		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return CoreUtility.ExecuteFunction<nfloat>("GetHeightForHeader", delegate ()
			{
				if (this.CustomHeaderSizerRaw != null)
				{
					return this.CustomHeaderSizerRaw(tableView, section);
				}
				if (this.CustomHeaderSizer != null)
				{
					object item = null;
					if (CreateSectionPerRow)
					{
						item = this.Items[(int)section];
					}
					return this.CustomHeaderSizer(tableView, item, section);
				}
				return this.DefaultHeaderHeight.GetValueOrDefault();
			});
		}
		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			return CoreUtility.ExecuteFunction<nfloat>("GetHeightForHeader", delegate ()
			{
				if (this.CustomFooterSizer != null)
				{
					return this.CustomFooterSizer(tableView, section);
				}
				return this.DefaultFooterHeight.GetValueOrDefault();
			});
		}
		public override string TitleForHeader(UITableView tableView, nint section)
		{
			return null;
		}

		public T GetItem<T>(int index)
		{
			return CoreUtility.ExecuteFunction<T>("GetItem", delegate ()
			{
				if (this.Items != null && this.Items.Count >= index)
				{
					return (T)this.Items[index];
				}
				return default(T);
			});
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			CoreUtility.ExecuteMethod("RowSelected", delegate ()
			{
				UITableViewCell cell = tableView.CellAt(indexPath);
				if (this.OnSelectedMethodRaw != null)
				{
					if (!this.DisableRowDeselection)
					{
						tableView.DeselectRow(indexPath, true); // iOS convention is to remove the highlight
					}
					if (this.OnSelectedMethodRaw != null)
					{
						this.OnSelectedMethodRaw(this, tableView, indexPath, cell);
					}
				}
				else
				{
					object info = this.GetItem<object>(indexPath.Row);
					if (!this.DisableRowDeselection)
					{
						tableView.DeselectRow(indexPath, true); // iOS convention is to remove the highlight
					}
					if (this.OnSelectedMethod != null)
					{
						this.OnSelectedMethod(info, indexPath, cell);
					}
				}
			});
		}

		public override void DraggingStarted(UIScrollView scrollView)
		{
			CoreUtility.ExecuteMethod("DraggingStarted", delegate ()
			{
				if (this.AutoHideTabBarController != null)
				{
					this.TabBarLastOffsetY = scrollView.ContentOffset.Y;
				}
			});
		}

		protected virtual void Scrolled_HandleTabBarHiding(UIScrollView scrollView)
		{
			CoreUtility.ExecuteMethod("Scrolled_HandleTabBarHiding", delegate ()
			{
				if (this.AutoHideTabBarController == null)
				{
					return; // ------- ShortCircuit
				}
				if (this.AutoHideControllerInstance != null)
				{
					if (!this.AutoHideControllerInstance.IsControllerVisible)
					{
						return;
					}
				}
				bool contentGoingUp = false;
				bool contentGoingDown = false;
				if (this.TabBarLastOffsetY < scrollView.ContentOffset.Y)
				{
					contentGoingUp = true;
				}
				else if (this.TabBarLastOffsetY > scrollView.ContentOffset.Y)
				{
					contentGoingDown = true;
				}

				if (!contentGoingUp && !contentGoingDown)
				{
					this.DoubleVerifyTabBar();
					return; // ------- ShortCircuit
				}
				if (contentGoingUp && AutoHideTabBarShouldBeHidden)
				{
					this.DoubleVerifyTabBar();
					return; // ------- ShortCircuit
				}
				if (contentGoingDown && !AutoHideTabBarShouldBeHidden)
				{
					this.DoubleVerifyTabBar();
					return; // ------- ShortCircuit
				}
				if (contentGoingUp)
				{
					AnimateTabBar(false);
				}
				else if (contentGoingDown)
				{
					AnimateTabBar(true);
				}

			}, null, true);
		}
		/// <summary>
		/// Cloned in CoreCollectionSource
		/// </summary>
		public virtual void AnimateTabBar(bool show, bool suspendTrackingDuringAnimation = false, bool force = false)
		{
			CoreUtility.ExecuteMethod("AnimateTabBar", delegate ()
			{
				if (this.AutoHideTabBarController == null) { return; }

				if (this.AutoHideControllerInstance != null)
				{
					if (!this.AutoHideControllerInstance.IsControllerVisible)
					{
						return;
					}
				}
				if (!force && (this.SuspendTabBarTracking || this.DisableTabBarTracking))
				{
					return;
				}
				UITabBar tabBar = this.AutoHideTabBarController.TabBar;

				if (!OriginalTabBarFrame.HasValue)
				{
					OriginalTabBarFrame = tabBar.Frame;
				}

				tabBar.Layer.RemoveAnimation(DRAG_ANIMATION_NAME);

				if (suspendTrackingDuringAnimation)
				{
					this.SuspendTabBarTracking = true;
				}
				CATransaction.Begin();
				CATransaction.CompletionBlock = new Action(delegate ()
				{
					if (suspendTrackingDuringAnimation) // only ours
					{
						this.SuspendTabBarTracking = false;
					}
					this.BeginInvokeOnMainThread(delegate ()
					{
						CoreUtility.ExecuteMethod("DraggingStarted.CompletionBlock", delegate ()
						{
							if (AutoHideTabBarShouldBeHidden)
							{
								tabBar.Frame = new CGRect(tabBar.Frame.X, this.AutoHideTabBarController.View.Frame.Height, tabBar.Frame.Width, tabBar.Frame.Height);
							}
							else
							{
								tabBar.Frame = OriginalTabBarFrame.GetValueOrDefault();
							}
							tabBar.Layer.RemoveAnimation(DRAG_ANIMATION_NAME);
						});
					});
				});


				if (show)
				{
					// content going down
					AutoHideTabBarShouldBeHidden = false;
					_hadAnimation = true;
					AnimationBuilder
						.Begin(tabBar.Layer, DRAG_ANIMATION_NAME)
						.MoveTo(OriginalTabBarFrame.Value.Location, 0f, 0.3f)
						.Commit();
				}
				else
				{
					// content going up
					AutoHideTabBarShouldBeHidden = true;
					_hadAnimation = true;
					AnimationBuilder
						.Begin(tabBar.Layer, DRAG_ANIMATION_NAME)
						.MoveTo(new CGPoint(tabBar.Layer.Frame.X, this.AutoHideTabBarController.View.Frame.Height), 0f, 0.3f)
						.Commit();
				}

				CATransaction.Commit();
			});
		}
		private bool _hadAnimation;
		protected void DoubleVerifyTabBar()
		{
			CoreUtility.ExecuteMethod("DoubleVerifyTabBar", delegate ()
			{
				if (!_hadAnimation) { return; }
				if (this.AutoHideTabBarController == null) { return; }
				if (this.AutoHideControllerInstance != null && !this.AutoHideControllerInstance.IsControllerVisible)
				{
					return;
				}

				UITabBar tabBar = this.AutoHideTabBarController.TabBar;
				if (tabBar.Layer.AnimationKeys == null || !tabBar.Layer.AnimationKeys.Contains(DRAG_ANIMATION_NAME))
				{
					if (!AutoHideTabBarShouldBeHidden)
					{
						tabBar.Frame = OriginalTabBarFrame.GetValueOrDefault();
					}
					_hadAnimation = false;
				}
			});
		}
		public override void Scrolled(UIScrollView scrollView)
		{
			// don't wrap for performance
			if (this.ScrollListener != null)
			{
				this.ScrollListener.OnScrolled(scrollView);
			}

			this.Scrolled_HandleTabBarHiding(scrollView);

			this.OnViewScrolled(scrollView);
		}

		public void AppendItems(IEnumerable items)
		{
			CoreUtility.ExecuteMethod("AppendItems", delegate ()
			{
				foreach (var item in items)
				{
					this.Items.Add(item);
				}
			});
		}
		public void ClearItems()
		{
			CoreUtility.ExecuteMethod("ClearItems", delegate ()
			{
				this.Items.Clear();
			});
		}


		// Fluent Interface
		public CoreTableSource For(IEnumerable data)
		{
			this.Items = new List<object>();
			foreach (var item in data)
			{
				this.Items.Add(item);
			}
			return this;
		}
		public CoreTableSource WhenLayoutOutCell(Action<UITableView, UITableViewCell, NSIndexPath> cellLayout)
		{
			this.CustomCellLayout = cellLayout;
			return this;
		}
		public CoreTableSource WhenCreatingCell(Func<UITableView, NSIndexPath, UITableViewCell> creator)
		{
			this.CustomCreatorRaw = creator;
			return this;
		}
		public CoreTableSource WhenCreatingCell<T>(Func<UITableView, T, NSIndexPath, UITableViewCell> creator)
			where T : class
		{
			this.CustomCreator = delegate (UITableView arg1, object arg2, NSIndexPath arg3)
			{
				return creator(arg1, (T)arg2, arg3);
			};
			return this;
		}
		public CoreTableSource WhenCreatingHeader(Func<CoreTableSource, UITableView, nint, UIView> creator)
		{
			this.CustomHeaderCreator = creator;
			return this;
		}
		public CoreTableSource WhenCreatingFooter(Func<CoreTableSource, UITableView, nint, UIView> creator)
		{
			this.CustomFooterCreator = creator;
			return this;
		}
		public CoreTableSource CreateSectionForEachRow(bool createForEachRow, nint? defaultHeaderHeight = null, nint? defaultFooterHeight = null)
		{
			this.DefaultHeaderHeight = defaultHeaderHeight;
			this.DefaultFooterHeight = defaultFooterHeight;
			this.CreateSectionPerRow = createForEachRow;
			return this;
		}
		public CoreTableSource WhenBindingCell<T>(Action<UITableViewCell, T, NSIndexPath> binder)
		{
			this.CustomBinding = delegate (UITableViewCell arg1, object arg2, NSIndexPath arg3)
			{
				binder(arg1, (T)arg2, arg3);
			};
			return this;
		}
		public CoreTableSource WhenSizingRows<T>(Func<UITableView, T, NSIndexPath, nfloat> rowSizer)
		{
			this.CustomRowSizer = delegate (UITableView arg1, object arg2, NSIndexPath arg3)
			{
				return rowSizer(arg1, (T)arg2, arg3);
			};
			return this;
		}
		public CoreTableSource WhenSizingRows(Func<UITableView, NSIndexPath, nfloat> rowSizer)
		{
			this.CustomRowSizerRaw = rowSizer;
			return this;
		}

		public CoreTableSource WhenDeletingRow(Func<UITableView, NSIndexPath, bool> canDelete, Action<UITableView, NSIndexPath> performDelete, Func<UITableView, NSIndexPath, string> deleteText = null)
		{
			this.CustomCanDelete = canDelete;
			this.CustomRowDelete = performDelete;
			this.CustomRowDeleteText = deleteText;
			return this;
		}

		public CoreTableSource WhenSizingHeaders(Func<UITableView, nint, nfloat> headerSizer)
		{
			this.CustomHeaderSizerRaw = headerSizer;
			return this;
		}
		public CoreTableSource WhenSizingHeaders<T>(Func<UITableView, T, nint, nfloat> headerSizer)
		{
			this.CustomHeaderSizer = delegate (UITableView arg1, object arg2, nint arg3)
			{
				return headerSizer(arg1, (T)arg2, arg3);
			};
			return this;
		}
		public CoreTableSource WhenSizingFooters(Func<UITableView, nint, nfloat> headerSizer)
		{
			this.CustomFooterSizer = delegate (UITableView arg1, nint arg3)
			{
				return headerSizer(arg1, arg3);
			};
			return this;
		}

		public CoreTableSource WhenCountingSections(Func<UITableView, nint> countSections)
		{
			CustomNumberOfSections = countSections;
			return this;
		}
		public CoreTableSource WhenCountingRowsInSection(Func<UITableView, nint, nint> countRowsInSections)
		{
			CustomRowsInSection = countRowsInSections;
			return this;
		}


		public CoreTableSource WhenScrolling(IScrollListener scrollListener)
		{
			this.ScrollListener = scrollListener;
			return this;
		}
		public CoreTableSource HideTabsWhenScrollingDown(BaseUIViewController host, UITabBarController controller, bool monitorControllerSelected, CGRect? originalTabBarFrame = null)
		{
			if (controller == null)
			{
				return this;
			}
			this.AutoHideControllerInstance = host;
			if (this.AutoHideTabBarController != null)
			{
				this.AutoHideTabBarController.ViewControllerSelected -= AutoHideTabBar_ControllerSelected;
			}
			this.AutoHideTabBarController = controller;
			this.AutoHideTabBarController.ViewControllerSelected -= AutoHideTabBar_ControllerSelected;//jic
			if (monitorControllerSelected)
			{
				this.AutoHideTabBarController.ViewControllerSelected += AutoHideTabBar_ControllerSelected;
			}
			CGRect frame = this.AutoHideTabBarController.TabBar.Frame;
			this.AutoHideTabBarController.TabBar.Layer.AnchorPoint = new CGPoint(0, 0);
			this.AutoHideTabBarController.TabBar.Frame = frame;
			if (originalTabBarFrame.HasValue)
			{
				this.OriginalTabBarFrame = originalTabBarFrame;
			}
			else
			{
				this.OriginalTabBarFrame = this.AutoHideTabBarController.TabBar.Frame;
			}
			return this;
		}

		private void AutoHideTabBar_ControllerSelected(object sender, UITabBarSelectionEventArgs e)
		{
			CoreUtility.ExecuteMethod("AutoHideTabBar_ControllerSelected", delegate ()
			{
				if (e.ViewController == AutoHideControllerInstance)
				{
					this.AnimateTabBar(true, false, true);// safety net
				}
			});
		}
		public CoreTableSource WhenItemSelected<T, K>(Action<T, K> selectedMethod)
			where K : UITableViewCell
		{
			this.OnSelectedMethod = delegate (object item, NSIndexPath index, UITableViewCell cell)
			{
				selectedMethod((T)item, (K)cell);
			};
			return this;
		}
		public CoreTableSource WhenItemSelectedRaw(Action<CoreTableSource, UITableView, NSIndexPath, UITableViewCell> selectedMethod)
		{
			this.OnSelectedMethodRaw = selectedMethod;
			return this;
		}

		public static CoreTableSource Build<T>(IEnumerable items, string defaultCellIdentifier, Action<T, NSIndexPath, UITableViewCell> onSelected, Action<UITableViewCell, T, NSIndexPath> binding = null)
		{
			CoreTableSource result = new CoreTableSource(items, defaultCellIdentifier, delegate (object t, NSIndexPath path, UITableViewCell cell)
			{
				if (onSelected != null)
				{
					onSelected((T)t, path, cell);
				}
			}
				, delegate (UITableViewCell cell, object item, NSIndexPath path)
			{
				if (binding != null)
				{
					binding(cell, (T)item, path);
				}
			});

			return result;
		}
	}
}
