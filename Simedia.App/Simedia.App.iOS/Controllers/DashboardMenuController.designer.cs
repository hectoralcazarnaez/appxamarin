// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("DashboardMenuController")]
	partial class DashboardMenuController
	{
		[Outlet]
		UIKit.UILabel lblCompanyName { get; set; }

		[Outlet]
		UIKit.UITableView tblData { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tblData != null) {
				tblData.Dispose ();
				tblData = null;
			}

			if (lblCompanyName != null) {
				lblCompanyName.Dispose ();
				lblCompanyName = null;
			}
		}
	}
}
