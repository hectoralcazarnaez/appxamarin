﻿using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace Simedia.App.iOS
{
    public class SimplePopup : BaseUIView
    {
        public static SimplePopup ShowList(UIView referenceControl, bool underLay, float itemHeight, params string[] items)
        {
            SimplePopup result = null;
            referenceControl.InvokeOnMainThread(delegate ()
            {
                try
                {
                    SimplePopup dropDown = new SimplePopup();
                    dropDown.Initialize(referenceControl, underLay, itemHeight, items);
                    HUD.ShowModal(dropDown);
                    result = dropDown;
                }
                catch
                {
                }
            });
            return result;
        }
        public static SimplePopup Show(UIView invokingControl, bool underLay, UIView view)
        {
            SimplePopup result = null;
            invokingControl.InvokeOnMainThread(delegate ()
            {
                try
                {
                    SimplePopup dropDown = new SimplePopup();
                    dropDown.Initialize(underLay, view);
                    HUD.ShowModal(dropDown);
                    result = dropDown;
                }
                catch
                {
                }
            });
            return result;
        }

        public SimplePopup()
            : base()
        {
        }


        private UIView _contentView;
        private UIView _referenceControl;
        private UIScrollView _scrollView;
        private List<UIView> _items = new List<UIView>();
        private List<UIView> _dividers = new List<UIView>();
        private TopBubbleMark _tickView;

        public void Dismiss()
        {
            base.ExecuteMethod("Dismiss", delegate ()
            {
                this.RemoveFromSuperview();
            });
        }
        public void Initialize(bool underLay, UIView view)
        {
            base.ExecuteMethod("Initialize", delegate ()
            {
                this.Frame = UIApplication.SharedApplication.KeyWindow.Bounds;
                _contentView = view;

                if (underLay)
                {
                    this.BackgroundColor = UIColor.FromWhiteAlpha(0, 0.6f);
                }
                else
                {
                    this.Layer.ShadowRadius = 4.0f;
                    this.Layer.ShadowOpacity = 0.5f;
                    this.Layer.ShadowOffset = new CGSize();
                    this.Layer.ShadowColor = UIColor.Black.CGColor;
                }
                this.AddSubview(_contentView);
            });
        }
        public void Initialize(UIView referenceControl, bool underLay, float itemHeight, string[] items)
        {
            base.ExecuteMethod("Initialize", delegate ()
            {
                _referenceControl = referenceControl;

                if (underLay)
                {
                    this.BackgroundColor = UIColor.FromWhiteAlpha(0, 0.6f);
                }
                else
                {
                    this.Layer.ShadowRadius = 4.0f;
                    this.Layer.ShadowOpacity = 0.5f;
                    this.Layer.ShadowOffset = new CGSize();
                    this.Layer.ShadowColor = UIColor.Black.CGColor;
                }

                _tickView = new TopBubbleMark(new CGRect(180, 0, 22f, 14f));
                this.AddSubview(_tickView);

                float computedHeight = 20 + (items.Length * itemHeight);
                if (computedHeight > 350)
                {
                    computedHeight = 350;
                }
                _scrollView = new UIScrollView(new CGRect(0, 8, 220, computedHeight));
                _scrollView.BackgroundColor = CoreAssumptions.COLOR_GREY_DARK.ConvertHexToColor();
                _scrollView.Layer.BorderColor = CoreAssumptions.COLOR_GREY_LIGHTER.ConvertHexToColor().CGColor;
                _scrollView.Layer.BorderWidth = 2f;
                _scrollView.Layer.CornerRadius = 10f;

                _contentView = new UIView(new CGRect(0, 0, _scrollView.Frame.Width, 20 + (items.Length * itemHeight)));


                for (int i = 0; i < items.Length; i++)
                {
                    UILabel label = new UILabel();
                    label.Frame = new CGRect(15, 5 + (itemHeight * i), _contentView.Frame.Width - 30, itemHeight);
                    label.AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin;
                    label.TextAlignment = UITextAlignment.Center;
                    label.Font = UIFont.SystemFontOfSize(12);
                    label.Lines = 0;
                    label.LineBreakMode = UILineBreakMode.WordWrap;
                    label.Text = items[i];
                    label.TextColor = UIColor.White;

                    _items.Add(label);

                    _contentView.AddSubview(label);

                    if (i > 0)
                    {
                        UIView divider = new UIView(new CGRect(0, label.Frame.Top, _contentView.Frame.Width, 0.5f));
                        divider.BackgroundColor = UIColor.LightGray;
                        divider.AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin;
                        _dividers.Add(divider);
                        _contentView.AddSubview(divider);
                    }

                }

                _scrollView.ContentSize = _contentView.Frame.Size;
                _scrollView.AddSubview(_contentView);
                this.AddSubview(_scrollView);
            });
        }

        public override void MovedToSuperview()
        {
            base.ExecuteMethod("MovedToSuperview", delegate ()
            {
                base.MovedToSuperview();

                UIView parent = this.Superview;
                if (parent != null)
                {
                    this.Frame = new CGRect(0, 0, parent.Bounds.Width, parent.Bounds.Height);
                    if (_referenceControl != null)
                    {
                        CGPoint bottomRightOfReference = _referenceControl.ConvertPointToView(new CGPoint(_referenceControl.Bounds.Width, _referenceControl.Bounds.Height), parent);
                        _tickView.Frame = new CGRect(bottomRightOfReference.X - 9 - _tickView.Frame.Width, bottomRightOfReference.Y + 4, _tickView.Frame.Width, _tickView.Frame.Height);
                        _scrollView.Frame = new CGRect(bottomRightOfReference.X - _scrollView.Frame.Width + 3, _tickView.Frame.Bottom, _scrollView.Frame.Width, _scrollView.Frame.Height);
                    }
                }
            });
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            base.ExecuteMethod("TouchesBegan", delegate ()
            {
                base.TouchesBegan(touches, evt);
                this.Dismiss();
            });
        }
    }
}
