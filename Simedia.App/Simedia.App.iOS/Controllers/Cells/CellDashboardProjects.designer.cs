// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellDashboardProjects")]
	partial class CellDashboardProjects
	{
		[Outlet]
		UIKit.UILabel lblIcon { get; set; }

		[Outlet]
		UIKit.UILabel lblProjectList { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblProjectList != null) {
				lblProjectList.Dispose ();
				lblProjectList = null;
			}

			if (lblIcon != null) {
				lblIcon.Dispose ();
				lblIcon = null;
			}
		}
	}
}
