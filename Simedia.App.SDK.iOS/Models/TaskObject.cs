﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Simedia.App.SDK.Models
{
    public partial class TaskObject
    {
		public TaskObject()
		{
			Activities = new List<Activity>();
		}

		public int task_id { get; set; }
		public int project_id { get; set; }
		public List<Activity> Activities { get; set; }
		[JsonIgnore]
		public string ui_token { get; set; }
        public bool IsItemExpanded { get; set; }
    }
}
