// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("LoginController")]
	partial class LoginController
	{
		[Outlet]
		UIKit.UIButton btnLogin { get; set; }

		[Outlet]
		UIKit.UILabel lblCompany { get; set; }

		[Outlet]
		UIKit.UILabel lblError { get; set; }

		[Outlet]
		UIKit.UITextField txtPassword { get; set; }

		[Outlet]
		UIKit.UITextField txtUserName { get; set; }

		[Outlet]
		UIKit.UIView vwBody { get; set; }

		[Outlet]
		UIKit.UIView vwHeader { get; set; }

		[Action ("btnLogin_Click:")]
		partial void btnLogin_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLogin != null) {
				btnLogin.Dispose ();
				btnLogin = null;
			}

			if (lblCompany != null) {
				lblCompany.Dispose ();
				lblCompany = null;
			}

			if (lblError != null) {
				lblError.Dispose ();
				lblError = null;
			}

			if (txtPassword != null) {
				txtPassword.Dispose ();
				txtPassword = null;
			}

			if (txtUserName != null) {
				txtUserName.Dispose ();
				txtUserName = null;
			}

			if (vwHeader != null) {
				vwHeader.Dispose ();
				vwHeader = null;
			}

			if (vwBody != null) {
				vwBody.Dispose ();
				vwBody = null;
			}
		}
	}
}
