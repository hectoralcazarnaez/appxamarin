// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("CellTasklog")]
	partial class CellTasklog
	{
		[Outlet]
		UIKit.UIButton btnTasklogEdit { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogComments { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogDate { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogId { get; set; }

		[Action ("btnTasklogEdit_Click:")]
		partial void btnTasklogEdit_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblTasklogId != null) {
				lblTasklogId.Dispose ();
				lblTasklogId = null;
			}

			if (lblTasklogDate != null) {
				lblTasklogDate.Dispose ();
				lblTasklogDate = null;
			}

			if (lblTasklogComments != null) {
				lblTasklogComments.Dispose ();
				lblTasklogComments = null;
			}

			if (btnTasklogEdit != null) {
				btnTasklogEdit.Dispose ();
				btnTasklogEdit = null;
			}
		}
	}
}
