﻿using System;
using System.Collections.Generic;

namespace Simedia.App
{
	public interface IDataViewModelView<T> : IViewModelView
	{
		void BindData(bool live_data, List<T> data);
		void AddData(List<T> data);
		void OnDataRefreshing(bool refreshing);
	}
}
