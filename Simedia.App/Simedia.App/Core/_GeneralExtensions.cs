﻿using System;
using System.Collections.Generic;
using System.Text;
using Simedia.App.SDK.Models;

namespace Simedia.App
{
    public static class _GeneralExtensions
    {
        public static T DisposeSafe<T>(this T item)
            where T : class, IDisposable
        {
            if (item != null)
            {
                item.Dispose();
            }
            return null;
        }
        public static bool ContainsKeySafe<Tkey, Tvalue>(this IDictionary<Tkey, Tvalue> item, Tkey key)
        {
            if (item != null)
            {
                return item.ContainsKey(key);
            }
            return false;
        }

        #region Number Methods

        public static string TruncateIfOver(this int value, int over, string formatIfNotEmpty = "{0}")
        {
            if (value > over)
            {
                return string.Format(formatIfNotEmpty, string.Format("{0}+", over));
            }
            else if (value > 0)
            {
                return string.Format(formatIfNotEmpty, value.ToString());
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Time Methods

        public static readonly DateTime EPOCH_START = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime ToDateTimeFromUnixSeconds(this int seconds)
        {
            return new DateTime(seconds * 10000000L + EPOCH_START.Ticks, DateTimeKind.Utc);
        }

        public static double ToUnixSeconds(this DateTime dateTime)
        {
            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - EPOCH_START.Ticks);
            double seconds = Math.Round(ts.TotalMilliseconds, 0) / 1000.0;
            return seconds;
        }
        public static string ToSeasonStartName(this DateTime dateTime)
        {
            switch (dateTime.Month)
            {
                case 8:
                case 9:
                case 10:
                    return "Fall";
                case 11:
                case 12:
                case 1:
                case 2:
                    return "Winter";
                case 3:
                case 4:
                case 5:
                    return "Spring";
                case 6:
                case 7:
                    return "Summer";
                default:
                    return "";
            }
        }
        public static string ToShortDateRange(this int unixTimeStampStart, int unixTimeStampEnd)
        {
            return ToShortDateRange(unixTimeStampStart.ToDateTimeFromUnixSeconds(), unixTimeStampEnd.ToDateTimeFromUnixSeconds());
        }
        public static string ToShortDateRange(this DateTime start, DateTime end)
        {
            if (start.Year == end.Year)
            {
                if (start.Month == end.Month)
                {
                    return string.Format("{0:MMM d} - {1:MMM d, yyyy}", start, end);
                }
                else
                {
                    return string.Format("{0:MMM d} - {1:MMM d, yyyy}", start, end);
                }
            }
            else
            {
                return string.Format("{0:MMM d, yyyy} - {1:MMM d, yyyy}", start, end);
            }
        }
        public static string ToLongDateRange(this int unixTimeStampStart, int unixTimeStampEnd)
        {
            return ToShortDateRange(unixTimeStampStart.ToDateTimeFromUnixSeconds(), unixTimeStampEnd.ToDateTimeFromUnixSeconds());
        }
        public static string ToLongDateRange(this DateTime start, DateTime end)
        {
            if (start.Year == end.Year)
            {
                if (start.Month == end.Month)
                {
                    return string.Format("{0:MMMM d} - {1:d, yyyy}", start, end);
                }
                else
                {
                    return string.Format("{0:MMMM d} - {1:MMMM d, yyyy}", start, end);
                }
            }
            else
            {
                return string.Format("{0:MMMM d, yyyy} - {1:MMMM d, yyyy}", start, end);
            }
        }
        public static string ToTimeAgoLongSmartUTC(this DateTime? dateTimeUTC)
        {
            if (dateTimeUTC.HasValue)
            {
                return dateTimeUTC.Value.ToTimeAgoLongSmartUTC();
            }
            return string.Empty;
        }
        public static string ToTimeAgoLongSmartUTC(this DateTime dateTimeUTC)
        {
            TimeSpan difference = (DateTime.UtcNow - dateTimeUTC);
            if (difference.TotalSeconds < 10)
            {
                return "just now";
            }
            if (difference.TotalDays <= 7)
            {
                return dateTimeUTC.ToTimeAgoLongUTC();
            }
            if (difference.TotalDays < 365)
            {
                return dateTimeUTC.ToString("MMM d");
            }
            return dateTimeUTC.ToString("M/d/yy");
        }
        /// <summary>
        /// Shows minutes/hours/seconds as long as its within a reasonable time frame
        /// Otherwise, shows a smart representation
        /// </summary>
        public static string ToTimeAgoShortSmartUTC(this DateTime? dateTimeUTC, bool includeAgo = false)
        {
            if (dateTimeUTC.HasValue)
            {
                return dateTimeUTC.Value.ToTimeAgoShortSmartUTC(includeAgo);
            }
            return string.Empty;
        }
        /// <summary>
        /// Shows minutes/hours/seconds as long as its within a reasonable time frame
        /// Otherwise, shows the date in UTC
        /// </summary>
        public static string ToTimeAgoShortSmartUTC(this DateTime dateTimeUTC, bool includeAgo = false)
        {
            TimeSpan difference = (DateTime.UtcNow - dateTimeUTC);
            if (difference.TotalSeconds < 10)
            {
                return "now";
            }
            if (difference.TotalDays <= 7)
            {
                return dateTimeUTC.ToTimeAgoShortUTC(includeAgo);
            }
            if (difference.TotalDays < 365)
            {
                return dateTimeUTC.ToString("MMM d");
            }
            return dateTimeUTC.ToString("M/d/yy");
        }

        /// <summary>
        /// Shows minutes/hours/seconds as long as its within a reasonable time frame
        /// Otherwise, shows the date in its timezone
        /// </summary>
        public static string ToTimeAgoShortSmart(this DateTimeOffset? dateTime)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.UtcDateTime.ToTimeAgoShortSmartUTC();
            }
            return string.Empty;
        }
        /// <summary>
        /// Shows minutes/hours/seconds as long as its within a reasonable time frame
        /// Otherwise, shows the date in its timezone
        /// </summary>
        public static string ToTimeAgoShortSmart(this DateTimeOffset dateTime)
        {
            return dateTime.UtcDateTime.ToTimeAgoShortSmartUTC();
        }
        public static string ToTimeAgoShortUTC(this DateTime dateTimeUTC, bool includeAgo = false)
        {
            DateTime now = DateTime.UtcNow;
            TimeSpan span = default(TimeSpan);
            string suffix = string.Empty;
            if (now > dateTimeUTC)
            {
                // in the past
                span = now - dateTimeUTC;
                if (includeAgo)
                {
                    suffix = " ago";
                }
            }
            else
            {
                // in the future
                span = dateTimeUTC - now;
            }
            int totalDays = (int)Math.Truncate(span.TotalDays);

            if (span.TotalMinutes < 1)
            {
                return string.Format("{0}s{1}", (int)span.TotalSeconds, suffix);
            }
            else if (span.TotalHours < 1)
            {
                return string.Format("{0}m{1}", (int)span.TotalMinutes, suffix);
            }
            else if (span.TotalHours < 24)
            {
                return string.Format("{0}h {1}m{2}", span.Hours, span.Minutes, suffix);
            }
            else if (span.TotalDays < 1)
            {
                return string.Format("{0}hrs{1}", span.Hours, suffix);
            }
            else if (totalDays >= 2)
            {
                return string.Format("{0}d{1}", totalDays, suffix);
            }
            else
            {
                return string.Format("{0}d {1}h{2}", totalDays, span.Hours, suffix);
            }
        }
        public static string ToTimeAgoLong(this DateTime dateTime)
        {
            return FormatTimeAgo(DateTime.Now, dateTime);
        }
        public static string ToTimeAgoLongUTC(this DateTime dateTimeUTC)
        {
            return FormatTimeAgo(DateTime.UtcNow, dateTimeUTC);
        }
        public static string FormatTimeAgo(DateTime now, DateTime dateTime)
        {
            StringBuilder sb = new StringBuilder();
            string agoSuffix = " ago";
            TimeSpan timespan = now - dateTime;

            if (now <= dateTime)
            {
                sb.Append("in ");
                timespan = dateTime - now;
            }

            // A year or more?  Do "[Y] years and [M] months ago"
            if ((int)timespan.TotalDays >= 365)
            {
                // Years
                int nYears = (int)Math.Truncate(timespan.TotalDays / 365);
                sb.Append(nYears);
                if (nYears > 1)
                    sb.Append(" years");
                else
                    sb.Append(" year");

                // Months
                int remainingDays = (int)timespan.TotalDays - (nYears * 365);
                int nMonths = (int)Math.Truncate(remainingDays / (double)30);
                if (nMonths == 1)
                    sb.Append(" and ").Append(nMonths).Append(" month");
                else if (nMonths > 1)
                    sb.Append(" and ").Append(nMonths).Append(" months");
            }
            // More than 60 days? (appx 2 months or 8 weeks)
            else if ((int)timespan.TotalDays >= 60)
            {
                // Do months
                int nMonths = (int)Math.Truncate(timespan.TotalDays / 30);
                sb.Append(nMonths).Append(" months");
            }
            // Weeks? (7 days or more)
            else if ((int)timespan.TotalDays >= 7)
            {
                int nWeeks = (int)Math.Truncate(timespan.TotalDays / 7);
                sb.Append(nWeeks);
                if (nWeeks == 1)
                    sb.Append(" week");
                else
                    sb.Append(" weeks");
            }
            // Days? (1 or more)
            else if ((int)timespan.TotalDays >= 1)
            {
                int nDays = (int)timespan.TotalDays;
                sb.Append(nDays);
                if (nDays == 1)
                    sb.Append(" day");
                else
                    sb.Append(" days");
            }
            // Hours?
            else if ((int)timespan.TotalHours >= 1)
            {
                int nHours = (int)Math.Truncate(timespan.TotalHours);
                sb.Append(nHours);
                if (nHours == 1)
                    sb.Append(" hour");
                else
                    sb.Append(" hours");
            }
            // Minutes?
            else if ((int)timespan.TotalMinutes >= 1)
            {
                int nMinutes = (int)Math.Truncate(timespan.TotalMinutes);
                sb.Append(nMinutes);
                if (nMinutes == 1)
                    sb.Append(" min");
                else
                    sb.Append(" mins");
            }
            // Seconds?
            else if ((int)timespan.TotalSeconds >= 1)
            {
                int nSeconds = (int)Math.Truncate(timespan.TotalSeconds);
                sb.Append(nSeconds);
                if (nSeconds == 1)
                    sb.Append(" sec");
                else
                    sb.Append(" secs");
            }
            // Just say "1 second" as the smallest unit of time
            else
            {
                sb.Append("1 sec");
            }

            if (now > dateTime)
            {
                sb.Append(agoSuffix);
            }


            // For anything more than 6 months back, put " ([Month] [Year])" at the end, for better reference
            if ((int)timespan.TotalDays >= 30 * 6)
            {
                sb.Append(" (" + dateTime.ToString("MMMM") + " " + dateTime.Year + ")");
            }

            return sb.ToString();
        }


        #endregion

        public static string ToOrdinalEnglish(this int number, string zeroText = "")
        {
            if (number == 0)
            {
                return zeroText;
            }

            string rankString = number.ToString();

            if (rankString.EndsWith("11")) { return rankString + "th"; }
            if (rankString.EndsWith("12")) { return rankString + "th"; }
            if (rankString.EndsWith("13")) { return rankString + "th"; }
            if (rankString.EndsWith("1")) { return rankString + "st"; }
            if (rankString.EndsWith("2")) { return rankString + "nd"; }
            if (rankString.EndsWith("3")) { return rankString + "rd"; }

            return rankString + "th";
        }

        private const string COUNT_SUFFIX = "kmb";
        public static string ToShortCount(this long count)
        {
            return FormatShortCount((ulong)count);
        }
        public static string ToShortCount(this uint count)
        {
            return FormatShortCount(count);
        }
        public static string ToShortCount(this int count)
        {
            return FormatShortCount((ulong)count);
        }
        public static string ToShortCount(this ulong count)
        {
            return FormatShortCount(count);
        }
        public static string FormatShortCount(ulong count)
        {
            int sections = (int)Math.Ceiling((double)count.ToString("####").Length / 3.0);
            if (sections <= 1)
            {
                return count.ToString(); // 987
            }
            double shifted = (count / Math.Pow(10, (sections - 1) * 3));
            shifted = Math.Truncate(shifted * 10) / 10;
            int ix = (sections - 2);
            if (ix < COUNT_SUFFIX.Length)
            {
                return shifted.ToString() + COUNT_SUFFIX[ix];
            }
            else
            {
                return ">999" + COUNT_SUFFIX[COUNT_SUFFIX.Length - 1];
            }
        }

        public static string ToReadableEnumName(this string enumName)
        {
			string itemText = enumName.Replace("__", ", ");
			itemText = itemText.Replace("_", " ");

            return itemText;
        }

		public static int GetAttachmentValue(this string itemText)
		{
            string enumName = itemText.Replace(", ", "__");
            enumName = enumName.Replace(" ", "_");

            int enumValue = (int)Enum.Parse(typeof(Attachment_Type), enumName);
            return enumValue;			
		}

        public static string GetAttachmentText(this int itemValue)
        {
            Attachment_Type attEnum = (Attachment_Type)itemValue;
            return attEnum.ToString();
        }
    }
}
