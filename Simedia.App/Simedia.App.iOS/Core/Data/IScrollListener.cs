﻿using System;
using UIKit;

namespace Simedia.App.iOS.Data
{
	public interface IScrollListener
	{
		void OnScrolled(UIScrollView scrollView);
		bool ListeningDisabled { get; set; }
	}
}
