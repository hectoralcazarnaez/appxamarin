﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Simedia.App.SDK.Models;

namespace Simedia.App.ViewModels
{
    public class TasklogViewModel : BaseViewModel
    {
        public TasklogViewModel(IViewModelView viewModelView, TaskLog tasklog)
            : base(viewModelView, "TasklogViewModel")
        {
            this.Tasklog = tasklog;
        }

        #region Language

        public string Text_ViewTitle_New
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_New, "NEW TASKLOG");
            }
        }

        public string Text_ViewTitle_Edit
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Edit, "EDIT TASKLOG");
            }
        }

        public string Text_TaskId
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_TaskId, "Task Id");
            }
        }

        public string Text_TasklogId
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Id, "ID");
            }
        }

        public string Text_TasklogDate
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Date, "Date");
            }
        }

        public string Text_TasklogHours
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Hours, "Hours");
            }
        }

        public string Text_TasklogRD
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_RD, "% R&D");
            }
        }

        public string Text_Comments
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Comments, "Comments");
            }
        }

        public string Text_Attachments
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Attachments, "Attachments");
            }
        }

        public string Text_AttachmentAdd
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_AttachmentAdd, "Add Attachment");
            }
        }

        public string Text_Save
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Save, "Save");
            }
        }

        public string Text_Cancel
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Cancel, "Cancel");
            }
        }

        public string Text_Close
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Close, "Close");
            }
        }

        public string Text_Done
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Done, "Done");
            }
        }

        public string Text_ChooseDate
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_ChooseDate, "Choose Date");
            }
        }

        public string Text_DateBlank
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_DateBlank, "Please enter date");
            }
        }

        public string Text_RDBlank
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_DateBlank, "Please enter R&D");
            }
        }

        public string Text_HoursBlank
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_HoursBlank, "Please enter hours");
            }
        }

        public string Text_Camera
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Camera, "Camera");
            }
        }

        public string Text_ImageLibrary
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_ChooseExistingImage, "Choose existing image");
            }
        }

        public string Text_VideoLibrary
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_ChooseExistingVideo, "Choose existing video");
            }
        }

        public string Text_Document
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Document, "Document");
            }
        }

        public string Text_AttachmentDescription
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_AttachmentDescription, "Please select attachment type");
            }
        }

        public string Text_NoCamera
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_NoCamera, "No camera");
            }
        }

        public string Text_NoCameraDescription
        {
            get
            {
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_NoCameraDescription, "No camera available");
            }
        }

		public string Text_Recording
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_Recording, "Recording...");
			}
		}

		public string Text_VoiceRecording
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_VoiceRecording, "Voice recording");
			}
		}

		public string Text_VoiceStopRecordingMessage
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_VoiceStopRecordingMessage, "Tap stop when finished");
			}
		}

		public string Text_VoiceRecordingStop
		{
			get
			{
				return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_VoiceRecordingStop, "Stop");
			}
		}

		public string Text_VoiceRecordingErrorMessage
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.TASKLOG_VoiceRecordingErrorMessage, "An error ocurred when trying to record.");
			}
		}

		public string Text_Error
		{
			get
			{
                return this.SimediaApp.GetLocalizedText(LanguageToken.GEN_Error, "Error");
			}
		}
        #endregion

        #region Public properties

        public TaskLog Tasklog { get; set; }

        public List<Attachment> Attachments
        {
            get
            {
                return this.Tasklog.attachments;
            }
        }

        public string TempPath
        {
            get
            {
                return this.SimediaApp.TempFolderPath;
            }
        }

        public string FileName
        {
            get
            {
                return $"Act_{this.Tasklog.activity_id}_Att_{this.Attachments.Count}";
            }
        }
        #endregion

        #region Overrides

        public override void Start()
        {
            base.ExecuteMethod("Start", delegate ()
            {
                base.Start();

                if (this.Tasklog == null)
                {
                    this.Tasklog = new TaskLog();
                }
                else
                {
                    if (this.Tasklog.attachments == null)
                    {
                        this.Tasklog.attachments = new List<Attachment>();
                    }
                }
            });
        }

        #endregion

        #region Public methods
        public void AddEditAttachment(Attachment attachment)
        {
            base.ExecuteMethod("AddEditAttachment", delegate ()
            {
				if (attachment.new_object)
				{
                    attachment.new_object = false;
					Attachments.Add(attachment);
				}
				else
				{
					Attachment storedAttachment = Attachments.Find(a => a.id == attachment.id);

					storedAttachment.description = attachment.description;
					storedAttachment.description_other = attachment.description_other;
				}
            });
        }

		public void RemoveAttachment(Attachment attachment)
		{
			base.ExecuteMethod("RemoveAttachment", delegate ()
            {
				Attachment storedAttachment = Attachments.Find(a => a.id == attachment.id);
                this.Attachments.Remove(storedAttachment);				
            });			
		}
        #endregion
    }
}
