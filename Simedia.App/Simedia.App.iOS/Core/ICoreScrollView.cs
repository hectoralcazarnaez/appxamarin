﻿using System;
namespace Simedia.App.iOS
{
	public interface ICoreScrollView
	{
		event EventHandler ViewScrolled;
	}
}
