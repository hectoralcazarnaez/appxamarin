﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UIKit;

namespace Simedia.App.iOS
{
    [Register("SimediaApplication")]
    public class SimediaApplication : UIApplication
    {
        public static string ContainerRoot
        {
            get
            {
                return Directory.GetParent(NSFileManager.DefaultManager.GetUrls(NSSearchPathDirectory.LibraryDirectory,
                        NSSearchPathDomain.User).First().Path).FullName;
            }
        }
        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            try
            {
                base.TouchesBegan(touches, evt);


                CGPoint location = (evt.AllTouches.AnyObject as UITouch).LocationInView(this.KeyWindow);
                if (UIApplication.SharedApplication.StatusBarFrame.Contains(location))
                {
                    SimediaAppDelegate.Current.OnStatusBarTouchesBegan(touches, evt);
                }
            }
            catch (Exception ex)
            {
                Container.Track.LogError(ex, "TouchesBegan");
            }
        }
    }
}
