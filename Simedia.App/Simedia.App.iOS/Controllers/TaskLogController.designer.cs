// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Simedia.App.iOS
{
	[Register ("TaskLogController")]
	partial class TaskLogController
	{
		[Outlet]
		UIKit.UIButton btnAttachment { get; set; }

		[Outlet]
		UIKit.UIButton btnCancel { get; set; }

		[Outlet]
		UIKit.UIButton btnMic { get; set; }

		[Outlet]
		UIKit.UIButton btnSave { get; set; }

		[Outlet]
		UIKit.UILabel lblAddAttachment { get; set; }

		[Outlet]
		UIKit.UILabel lblAttachment { get; set; }

		[Outlet]
		UIKit.UILabel lblComments { get; set; }

		[Outlet]
		UIKit.UILabel lblError { get; set; }

		[Outlet]
		UIKit.UILabel lblSpeechStep { get; set; }

		[Outlet]
		UIKit.UILabel lblTaskId { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogDate { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogHours { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogId { get; set; }

		[Outlet]
		UIKit.UILabel lblTasklogRD { get; set; }

		[Outlet]
		UIKit.UILabel lblTaskUser { get; set; }

		[Outlet]
		UIKit.UITableView tblAttachments { get; set; }

		[Outlet]
		UIKit.UITextView txtComments { get; set; }

		[Outlet]
		UIKit.UITextField txtTasklogDate { get; set; }

		[Outlet]
		UIKit.UITextField txtTasklogHours { get; set; }

		[Outlet]
		UIKit.UITextField txtTasklogId { get; set; }

		[Outlet]
		UIKit.UITextField txtTasklogRD { get; set; }

		[Outlet]
		UIKit.UIView vwErrorMsg { get; set; }

		[Action ("btnAttachment_Click:")]
		partial void btnAttachment_Click (Foundation.NSObject sender);

		[Action ("btnCancel_Click:")]
		partial void btnCancel_Click (Foundation.NSObject sender);

		[Action ("btnEditComment_Click:")]
		partial void btnEditComment_Click (Foundation.NSObject sender);

		[Action ("btnMic_Click:")]
		partial void btnMic_Click (Foundation.NSObject sender);

		[Action ("btnSave_Click:")]
		partial void btnSave_Click (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAttachment != null) {
				btnAttachment.Dispose ();
				btnAttachment = null;
			}

			if (btnCancel != null) {
				btnCancel.Dispose ();
				btnCancel = null;
			}

			if (btnSave != null) {
				btnSave.Dispose ();
				btnSave = null;
			}

			if (lblAddAttachment != null) {
				lblAddAttachment.Dispose ();
				lblAddAttachment = null;
			}

			if (lblAttachment != null) {
				lblAttachment.Dispose ();
				lblAttachment = null;
			}

			if (lblComments != null) {
				lblComments.Dispose ();
				lblComments = null;
			}

			if (lblError != null) {
				lblError.Dispose ();
				lblError = null;
			}

			if (lblTaskId != null) {
				lblTaskId.Dispose ();
				lblTaskId = null;
			}

			if (lblTasklogDate != null) {
				lblTasklogDate.Dispose ();
				lblTasklogDate = null;
			}

			if (lblTasklogHours != null) {
				lblTasklogHours.Dispose ();
				lblTasklogHours = null;
			}

			if (lblTasklogId != null) {
				lblTasklogId.Dispose ();
				lblTasklogId = null;
			}

			if (lblTasklogRD != null) {
				lblTasklogRD.Dispose ();
				lblTasklogRD = null;
			}

			if (lblTaskUser != null) {
				lblTaskUser.Dispose ();
				lblTaskUser = null;
			}

			if (tblAttachments != null) {
				tblAttachments.Dispose ();
				tblAttachments = null;
			}

			if (txtComments != null) {
				txtComments.Dispose ();
				txtComments = null;
			}

			if (txtTasklogDate != null) {
				txtTasklogDate.Dispose ();
				txtTasklogDate = null;
			}

			if (txtTasklogHours != null) {
				txtTasklogHours.Dispose ();
				txtTasklogHours = null;
			}

			if (txtTasklogId != null) {
				txtTasklogId.Dispose ();
				txtTasklogId = null;
			}

			if (txtTasklogRD != null) {
				txtTasklogRD.Dispose ();
				txtTasklogRD = null;
			}

			if (vwErrorMsg != null) {
				vwErrorMsg.Dispose ();
				vwErrorMsg = null;
			}

			if (btnMic != null) {
				btnMic.Dispose ();
				btnMic = null;
			}

			if (lblSpeechStep != null) {
				lblSpeechStep.Dispose ();
				lblSpeechStep = null;
			}
		}
	}
}
