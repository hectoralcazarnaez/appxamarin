﻿using Simedia.App.Services.MediaUploader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Simedia.App.iOS
{
    public class IOSMediaUploader : BaseMediaUploader
    {
        public IOSMediaUploader()
            : base("IOSMediaUploader")
        {
        }
    }
}
