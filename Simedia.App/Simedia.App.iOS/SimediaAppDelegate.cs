﻿using Foundation;
using Simedia.App.App;
using Simedia.App.Services.MediaUploader;
using System;
using UIKit;
using Simedia.App.Caching;
using Simedia.App.iOS.Caching;
using System.Net;
using System.IO;

namespace Simedia.App.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register ("SimediaAppDelegate")]
	public partial class SimediaAppDelegate : UIApplicationDelegate
	{
        #region Constructor
        public SimediaAppDelegate()
        {

        }
        #endregion

        #region Static Properties
        public static SimediaAppDelegate Current
        {
            get
            {
                return UIApplication.SharedApplication.Delegate as SimediaAppDelegate;
            }
        }
        #endregion

        #region Public Properties        
        public virtual UIWindow AppWindow { get; set; }
        public virtual UIStoryboard MainStoryboard { get; set; }
        public virtual object DeviceToken { get; set; }
        public virtual IDashboardController DashboardController { get; set; }
        #endregion

        #region Overrides
        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            string tempFolderPath = GetTempPath();
            string speechLocalization = "en_US";
            Container.Track = new IOSTrack();
            IViewPlatform viewPlatform = new IOSViewPlatform();
            ICacheFileStore fileStore = new IOSFileStore();
            ICacheHost cacheHost = new CacheHost(CoreAssumptions.INTERNAL_APP_NAME);
            IDataCache dataCache = new DataCache(cacheHost);
            ISimediaApp simediaApp = new SimediaApp(viewPlatform, CoreAssumptions.BASE_API_URL,cacheHost, dataCache, tempFolderPath, speechLocalization);
            IMediaUploader mediaUploader = new IOSMediaUploader();
            Container.RegisterDependencies(simediaApp, viewPlatform, mediaUploader, fileStore, cacheHost, dataCache);

            Container.SimediaApp.Initialize();

            this.MainStoryboard = UIStoryboard.FromName("Main", null);
            this.AppWindow = new UIWindow(UIScreen.MainScreen.Bounds);

            if (simediaApp.CurrentUser == null)
            {
                this.LaunchLogin();
            }
            else
            {
                this.LaunchDashboard();
            }

            this.AppWindow.MakeKeyAndVisible();

            return true;
        }

        public override UIWindow Window
        {
            get;
            set;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
        #endregion

        #region Public Methods
        public virtual void LaunchLogin(UIViewAnimationOptions transition = UIViewAnimationOptions.TransitionFlipFromLeft)
        {
            CoreUtility.ExecuteMethod("LaunchLogin", delegate ()
            {
                string identifier = "LoginController";
                if (!string.IsNullOrEmpty(CoreAssumptions.NAMING.LoginUI_IOS))
                {
                    identifier = CoreAssumptions.NAMING.LoginUI_IOS;
                }
                var firstController = this.MainStoryboard.InstantiateViewController(identifier);
                UINavigationController navController = new UINavigationController(firstController);
                navController.NavigationBarHidden = true;
                this.ChangeRootViewController(navController, transition);
            });
        }

        public virtual void LaunchDashboard(UIViewAnimationOptions transition = UIViewAnimationOptions.TransitionFlipFromRight)
        {
            CoreUtility.ExecuteMethod("LaunchSideMenu", delegate ()
            {
				DashboardController controller = new DashboardController();
				this.ChangeRootViewController(controller, transition);
            });
        }

		public virtual void LaunchProjectList(UIViewAnimationOptions transition = UIViewAnimationOptions.TransitionFlipFromRight)
		{
			CoreUtility.ExecuteMethod("LaunchProjectList", delegate ()
			{
				//ProjectListController projectList = this.MainStoryboard.InstantiateViewController(ProjectListController.IDENTIFIER) as ProjectListController;
				//this.PushViewControllerWithDisposeOnReturn(projectList, true);

				//ProjectListController controller = new ProjectListController();
				////this.ChangeRootViewController(controller, transition);
			});
		}
        #endregion

        #region Event Handlers
        public void OnStatusBarTouchesBegan(NSSet touches, UIEvent evt)
        {
            CoreUtility.ExecuteMethod("OnStatusBarTouchesBegan", delegate ()
            {
                //UIViewController topController = UIApplication.SharedApplication.GetRootVisibleController();
                //BaseUIViewController baseController = topController as BaseUIViewController;
                //if (baseController != null)
                //{
                //    baseController.ScrollToTop();
                //}
                //IDashboardController dashboardController = topController as IDashboardController;
                //if (dashboardController != null)
                //{
                //    dashboardController.ScrollToTop();
                //}
            });
        }
        #endregion

        #region Protected Methods
        protected virtual void ChangeRootViewController(UIViewController newController, UIViewAnimationOptions transition)
        {
            CoreUtility.ExecuteMethod("ChangeRootViewController", delegate ()
            {
                UIViewController previousRoot = this.AppWindow.RootViewController;
                if (this.AppWindow.RootViewController == null)
                {
                    this.AppWindow.RootViewController = newController;
                }
                else
                {
                    UIView.Transition(this.AppWindow, 0.5, transition, delegate ()
                    {
                        this.AppWindow.RootViewController = newController;
                    }, null);
                }
                if (previousRoot != null)
                {
                    previousRoot.Dispose();
                }
            });
        }

		protected string GetTempPath()
		{
			var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var ret = Path.Combine(documents, "..", "tmp");
			return ret;
		}
        #endregion
        
	}
}


